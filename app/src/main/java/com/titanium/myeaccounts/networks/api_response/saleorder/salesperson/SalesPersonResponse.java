package com.titanium.myeaccounts.networks.api_response.saleorder.salesperson;

import com.titanium.myeaccounts.networks.api_response.saleorder.status.Statuses;

public class SalesPersonResponse {
    private Statuses statuses;

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }
}
