package com.titanium.myeaccounts.networks.api_response.defaultitems;

public class CreateDefaultItemsResponse {
    public int status;
    public String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}