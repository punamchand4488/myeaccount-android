package com.titanium.myeaccounts.networks.api_response.unit;

public class UnitDetail {
    public UnitDetailData getData() {
        return data;
    }

    public void setData(UnitDetailData data) {
        this.data = data;
    }

    public UnitDetailData data;
}