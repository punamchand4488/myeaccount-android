package com.titanium.myeaccounts.networks.api_response.accountgroup;

public class AccountGroupDetails {
    public AccountGroupDetailsData getData() {
        return data;
    }

    public void setData(AccountGroupDetailsData data) {
        this.data = data;
    }

    public AccountGroupDetailsData data;

}