package com.titanium.myeaccounts.networks.api_request;

import android.content.Context;

import com.titanium.myeaccounts.activities.company.salepos.SaleOrderItemListActivity;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.Preferences;

import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by BerylSystems on 11/23/2017.
 */

public class RequestCreatePOSVoucher {
    public HashMap voucher;
    public HashMap items;
    public RequestCreatePOSVoucher(Context ctx){
       AppUser appUser = LocalRepositories.getAppUser(ctx);
       voucher = new HashMap<>();
        voucher.put("pos", appUser.pos_identifier);
        voucher.put("date", Preferences.getInstance(getApplicationContext()).getPos_date());
        voucher.put("voucher_series",Preferences.getInstance(getApplicationContext()).getVoucherSeries());
        voucher.put("voucher_number", Preferences.getInstance(getApplicationContext()).getVoucher_number());
        voucher.put("company_id", Preferences.getInstance(ctx).getCid());
        voucher.put("sale_type_id", Preferences.getInstance(getApplicationContext()).getPos_sale_type_id());
        voucher.put("payment_type", Preferences.getInstance(ctx).getCash_credit());
        voucher.put("account_master_id",Preferences.getInstance(ctx).getPos_party_id());
        voucher.put("shipped_to_id",Preferences.getInstance(ctx).getShipped_to_sale_order_id());
        voucher.put("mobile_number", Preferences.getInstance(getApplicationContext()).getPos_mobile());
        voucher.put("material_center_id",Preferences.getInstance(ctx).getPos_store_id());
        voucher.put("narration", Preferences.getInstance(ctx).getSale_order_narration());
        voucher.put("items", SaleOrderItemListActivity.mListMapForItemSale);
        voucher.put("bill_sundry_amount",appUser.billsundrytotal);
        voucher.put("bill_sundry", appUser.mListMapForBillSale);
        voucher.put("total", appUser.totalamount);
        voucher.put("items_amount", appUser.items_amount);
        voucher.put("bill_sundries_amount", appUser.bill_sundries_amount);
        voucher.put("send_email",appUser.email_yes_no);
        //voucher.put("attachment",appUser.sale_attachment);
        voucher.put("attachment", Preferences.getInstance(ctx).getSale_order_attachment());
        voucher.put("transport_details",SaleOrderItemListActivity.transport_details);
        voucher.put("payment_settlement",appUser.paymentSettlementHashMap);
        voucher.put("branch_id",Preferences.getInstance(getApplicationContext()).getBranch_id());
        voucher.put("user",appUser.cusername);
        voucher.put("sales_person_id",Preferences.getInstance(getApplicationContext()).getSale_person_id());
   }
}