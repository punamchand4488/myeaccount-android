package com.titanium.myeaccounts.networks.api_response.saleorder.branch;

public class BranchResponse {
    private Branches branches;

    public Branches getBranches() {
        return branches;
    }

    public void setBranches(Branches branches) {
        this.branches = branches;
    }
}
