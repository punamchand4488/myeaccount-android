package com.titanium.myeaccounts.networks.api_response.stock_journal;

public class Attributes {
    String date;
    String time;
    String voucher_series;
    String company_id;
    String id;
    String voucher_number;
    Double material_center_produced_id;
    Double material_center_consumed_id;
    Double total_amount;
    Double stock_consumed_quantity;
    Double stock_consumed_amount;
    Double stock_generated_quantity;
    Double stock_generated_amount;
    String invoice_html;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVoucher_series() {
        return voucher_series;
    }

    public void setVoucher_series(String voucher_series) {
        this.voucher_series = voucher_series;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVoucher_number() {
        return voucher_number;
    }

    public void setVoucher_number(String voucher_number) {
        this.voucher_number = voucher_number;
    }

    public Double getMaterial_center_produced_id() {
        return material_center_produced_id;
    }

    public void setMaterial_center_produced_id(Double material_center_produced_id) {
        this.material_center_produced_id = material_center_produced_id;
    }

    public Double getMaterial_center_consumed_id() {
        return material_center_consumed_id;
    }

    public void setMaterial_center_consumed_id(Double material_center_consumed_id) {
        this.material_center_consumed_id = material_center_consumed_id;
    }

    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    public Double getStock_consumed_quantity() {
        return stock_consumed_quantity;
    }

    public void setStock_consumed_quantity(Double stock_consumed_quantity) {
        this.stock_consumed_quantity = stock_consumed_quantity;
    }

    public Double getStock_consumed_amount() {
        return stock_consumed_amount;
    }

    public void setStock_consumed_amount(Double stock_consumed_amount) {
        this.stock_consumed_amount = stock_consumed_amount;
    }

    public Double getStock_generated_quantity() {
        return stock_generated_quantity;
    }

    public void setStock_generated_quantity(Double stock_generated_quantity) {
        this.stock_generated_quantity = stock_generated_quantity;
    }

    public Double getStock_generated_amount() {
        return stock_generated_amount;
    }

    public void setStock_generated_amount(Double stock_generated_amount) {
        this.stock_generated_amount = stock_generated_amount;
    }

    public String getInvoice_html() {
        return invoice_html;
    }

    public void setInvoice_html(String invoice_html) {
        this.invoice_html = invoice_html;
    }
}
