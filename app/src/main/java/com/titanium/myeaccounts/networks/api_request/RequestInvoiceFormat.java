package com.titanium.myeaccounts.networks.api_request;

import android.content.Context;

import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.utils.LocalRepositories;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abc on 7/12/2018.
 */

public class RequestInvoiceFormat {
    public Map<String, String> company;
    public RequestInvoiceFormat(Context ctx) {

        AppUser appUser = LocalRepositories.getAppUser(ctx);

        company = new HashMap<>();
        company.put("invoice_format",appUser.invoice_format.trim());

    }

}
