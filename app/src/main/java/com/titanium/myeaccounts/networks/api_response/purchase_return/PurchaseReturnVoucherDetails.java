package com.titanium.myeaccounts.networks.api_response.purchase_return;

public class PurchaseReturnVoucherDetails {
    public PurchaseReturnVoucherDetailsData getData() {
        return data;
    }

    public void setData(PurchaseReturnVoucherDetailsData data) {
        this.data = data;
    }

    public PurchaseReturnVoucherDetailsData data;
}