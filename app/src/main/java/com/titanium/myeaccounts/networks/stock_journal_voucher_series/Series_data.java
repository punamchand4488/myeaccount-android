package com.titanium.myeaccounts.networks.stock_journal_voucher_series;

public class Series_data {
    String voucher_number;
    String voucher_series;

    public String getVoucher_number() {
        return voucher_number;
    }

    public void setVoucher_number(String voucher_number) {
        this.voucher_number = voucher_number;
    }

    public String getVoucher_series() {
        return voucher_series;
    }

    public void setVoucher_series(String voucher_series) {
        this.voucher_series = voucher_series;
    }
}
