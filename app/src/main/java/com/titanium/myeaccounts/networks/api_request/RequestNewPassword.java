package com.titanium.myeaccounts.networks.api_request;

import android.content.Context;

import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.utils.LocalRepositories;

import java.util.HashMap;
import java.util.Map;


public class RequestNewPassword {
    public Map<String, String> user;



    public RequestNewPassword(Context ctx) {
        AppUser appUser = LocalRepositories.getAppUser(ctx);

        user = new HashMap<>();
        user.put("id", appUser.user_id);
        user.put("password", appUser.password);
    }
}
