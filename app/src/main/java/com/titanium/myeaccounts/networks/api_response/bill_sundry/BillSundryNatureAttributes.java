package com.titanium.myeaccounts.networks.api_response.bill_sundry;

public class BillSundryNatureAttributes {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String name;
}