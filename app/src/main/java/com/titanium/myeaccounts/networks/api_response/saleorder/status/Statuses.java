package com.titanium.myeaccounts.networks.api_response.saleorder.status;

import com.titanium.myeaccounts.networks.api_response.saleorder.branch.BranchesData;

import java.util.ArrayList;

public class Statuses {
    private ArrayList<BranchesData> data;

    public ArrayList<BranchesData> getData() {
        return data;
    }

    public void setData(ArrayList<BranchesData> data) {
        this.data = data;
    }
}
