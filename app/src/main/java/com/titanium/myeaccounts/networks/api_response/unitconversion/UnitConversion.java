package com.titanium.myeaccounts.networks.api_response.unitconversion;

import java.util.ArrayList;

public class UnitConversion {
    public ArrayList<UnitConversionData> getData() {
        return data;
    }

    public void setData(ArrayList<UnitConversionData> data) {
        this.data = data;
    }

    public ArrayList<UnitConversionData> data;

}