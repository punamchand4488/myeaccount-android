package com.titanium.myeaccounts.networks.api_response.purchasevoucher;

public class DeletePurchaseVoucherResponse {
    public String message;
    public int status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}