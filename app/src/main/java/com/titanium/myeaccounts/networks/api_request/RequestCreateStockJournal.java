package com.titanium.myeaccounts.networks.api_request;

import android.content.Context;
import android.content.Intent;

import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.Preferences;

import java.util.HashMap;

/**
 * Created by BerylSystems on 11/23/2017.
 */

public class RequestCreateStockJournal {
    public HashMap voucher;
    public HashMap items;
    public RequestCreateStockJournal(Context ctx){
       AppUser appUser = LocalRepositories.getAppUser(ctx);
       voucher = new HashMap<>();
        voucher.put("voucher_series", appUser.sale_series);
        voucher.put("material_center_consumed_id", Preferences.getInstance(ctx).getStoreId());
        voucher.put("material_center_produced_id", Preferences.getInstance(ctx).getStoreId2());
        voucher.put("date", appUser.sale_date);
        voucher.put("narration", appUser.sale_narration);
        voucher.put("voucher_number", appUser.sale_vchNo);
        voucher.put("stock_consumed_amount", Preferences.getInstance(ctx).getConsumedAmount().replace("Total Amount: ",""));
        int consumedQuantity = 0;
        for (int i=0;i<appUser.mListMapForItemSaleSJ.size();i++){
            consumedQuantity=consumedQuantity+Integer.valueOf((String) appUser.mListMapForItemSaleSJ.get(i).get("quantity"));
        }

        int generatedQuantity = 0;
        for (int i=0;i<appUser.mListMapForItemSale.size();i++){
            generatedQuantity=generatedQuantity+Integer.valueOf((String)appUser.mListMapForItemSale.get(i).get("quantity"));
        }
        voucher.put("stock_consumed_quantity", consumedQuantity);
        voucher.put("stock_generated_amount", Preferences.getInstance(ctx).getGeneratedAmount().replace("Total Amount: ",""));
        voucher.put("stock_generated_quantity", generatedQuantity);
        voucher.put("item_cart_consumed", appUser.mListMapForItemSaleSJ);
        voucher.put("item_cart_produced", appUser.mListMapForItemSale);

   }
}