package com.titanium.myeaccounts.networks.api_response.saleorder.status;

public class StatusesResponse {
    private Statuses statuses;

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }
}
