package com.titanium.myeaccounts.networks.api_response.salevoucher;

public class SaleVoucherDetails {
    public SaleVoucherDetailsData getData() {
        return data;
    }

    public void setData(SaleVoucherDetailsData data) {
        this.data = data;
    }

    public SaleVoucherDetailsData data;
}