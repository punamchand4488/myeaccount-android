package com.titanium.myeaccounts.networks.api_response.creditnotewoitem;

public class CreditNoteDetails {

    public CreditNoteDetailsData data;

    public CreditNoteDetailsData getData() {
        return data;
    }

    public void setData(CreditNoteDetailsData data) {
        this.data = data;
    }
}