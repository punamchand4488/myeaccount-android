package com.titanium.myeaccounts.networks.api_response.purchasevoucher;

public class PurchaseVoucherDetails {
    public PurchaseVoucherDetailsData getData() {
        return data;
    }

    public void setData(PurchaseVoucherDetailsData data) {
        this.data = data;
    }

    public PurchaseVoucherDetailsData data;
}