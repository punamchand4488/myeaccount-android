package com.titanium.myeaccounts.networks.api_response.materialcentre;

public class MaterialCentre {
    public GetMaterialCentreDetailsData getData() {
        return data;
    }

    public void setData(GetMaterialCentreDetailsData data) {
        this.data = data;
    }

    public GetMaterialCentreDetailsData data;
}