package com.titanium.myeaccounts.networks.stock_journal_voucher_series;

import com.titanium.myeaccounts.networks.api_response.voucherseries.VoucherSeries;

import java.util.ArrayList;

/**
 * Created by SAMSUNG on 8/2/2018.
 */

public class StockJournalVoucherSeriesResponse {
    int status;
    ArrayList<Series_data> series_data;

    public ArrayList<Series_data> getSeries_data() {
        return series_data;
    }

    public void setSeries_data(ArrayList<Series_data> series_data) {
        this.series_data = series_data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
