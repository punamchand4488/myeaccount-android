package com.titanium.myeaccounts.networks.api_response.receiptvoucher;

public class ReceiptVouchersDetails {

    public ReceiptVouchersDetailsData data;

    public ReceiptVouchersDetailsData getData() {
        return data;
    }

    public void setData(ReceiptVouchersDetailsData data) {
        this.data = data;
    }
}