package com.titanium.myeaccounts.networks.api_response.journalvoucher;

public class JournalVoucherDetails {

    public JournalVoucherDetailsData data;

    public JournalVoucherDetailsData getData() {
        return data;
    }

    public void setData(JournalVoucherDetailsData data) {
        this.data = data;
    }
}