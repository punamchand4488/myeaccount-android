package com.titanium.myeaccounts.networks.api_response.stock_journal;

import com.titanium.myeaccounts.networks.api_response.salevoucher.SaleVoucher;

public class GetStockJournalListResponse {
    public String message;
    public int status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Stock_journal getStock_journal() {
        return stock_journal;
    }

    public void setSale_vouchers(Stock_journal sale_vouchers) {
        this.stock_journal = sale_vouchers;
    }

    public Stock_journal stock_journal;
}