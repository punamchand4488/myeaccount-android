package com.titanium.myeaccounts.networks.api_response.itemgroup;

public class ItemGroupsDetails {
    public ItemGroupDetailsData data;

    public ItemGroupDetailsData getData() {
        return data;
    }

    public void setData(ItemGroupDetailsData data) {
        this.data = data;
    }
}