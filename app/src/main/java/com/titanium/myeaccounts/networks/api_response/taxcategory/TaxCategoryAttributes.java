package com.titanium.myeaccounts.networks.api_response.taxcategory;

public class TaxCategoryAttributes {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}