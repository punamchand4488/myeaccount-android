package com.titanium.myeaccounts.networks.api_response.saleorder.branch;

import com.titanium.myeaccounts.networks.api_response.pdc.Attribute;

public class BranchesData {
    private String type;
    private String id;
    private BranchAttribute attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BranchAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(BranchAttribute attributes) {
        this.attributes = attributes;
    }
}
