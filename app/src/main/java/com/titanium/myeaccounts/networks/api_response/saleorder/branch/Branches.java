package com.titanium.myeaccounts.networks.api_response.saleorder.branch;

import org.json.JSONObject;

import java.util.ArrayList;

public class Branches {
    private ArrayList<BranchesData> data;

    public ArrayList<BranchesData> getData() {
        return data;
    }

    public void setData(ArrayList<BranchesData> data) {
        this.data = data;
    }
}
