package com.titanium.myeaccounts.networks.api_response.bankcashwithdraw;

public class BankCashWithdrawDetails {

    public BankCashWithdrawDetailsData data;

    public BankCashWithdrawDetailsData getData() {
        return data;
    }

    public void setData(BankCashWithdrawDetailsData data) {
        this.data = data;
    }
}