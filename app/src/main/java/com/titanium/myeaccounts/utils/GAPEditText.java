package com.titanium.myeaccounts.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by silence12 on 21/8/17.
 */

@SuppressLint("AppCompatCustomView")
public class GAPEditText extends EditText {
    public GAPEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLongClickable(false);
        isInEditMode();
    }

    public GAPEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLongClickable(false);
        isInEditMode();
    }

    public GAPEditText(Context context) {
        super(context);
        setLongClickable(false);
        isInEditMode();
    }

    public void setTypeface(Typeface tf, int style) {
        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Serif.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Serif.ttf");
        if (style == 1) {
            super.setTypeface(boldTypeface);
        } else {
            super.setTypeface(normalTypeface);
        }
    }
}
