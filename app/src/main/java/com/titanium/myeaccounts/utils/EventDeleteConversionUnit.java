package com.titanium.myeaccounts.utils;

public class EventDeleteConversionUnit {
    private final int position;

    public EventDeleteConversionUnit(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}