package com.titanium.myeaccounts.utils;

public class EventDeleteUnit {
    private final int position;

    public EventDeleteUnit(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}