package com.titanium.myeaccounts.utils;

public class EventEditLogin {
    private final int position;

    public EventEditLogin(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}