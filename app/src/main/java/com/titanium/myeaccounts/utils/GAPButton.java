package com.titanium.myeaccounts.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


/**
 * Created by silence12 on 2/8/17.
 */

public class GAPButton extends android.support.v7.widget.AppCompatButton implements View.OnTouchListener {
    Drawable newColor;

    public GAPButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        isInEditMode();
        init();
    }

    public GAPButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        isInEditMode();
        init();
    }

    public GAPButton(Context context) {
        super(context);
        isInEditMode();
        init();
    }

    public void setTypeface(Typeface tf, int style) {
        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Serif.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Serif.ttf");
        if (style == 1) {
            super.setTypeface(boldTypeface);
        } else {
            super.setTypeface(normalTypeface);
        }
    }

    public void init() {
        setOnTouchListener(this);
        this.newColor = getBackground();
        this.newColor.setAlpha(170);
        getBackground().setAlpha(255);
    }

    private void turnOnImage() {
        int pL = getPaddingLeft();
        int pT = getPaddingTop();
        int pR = getPaddingRight();
        int pB = getPaddingBottom();
        TransitionDrawable trans = new TransitionDrawable(new Drawable[]{getBackground(), this.newColor});
        if (Build.VERSION.SDK_INT >= 16) {
            setBackground(trans);
        } else {
            setBackgroundDrawable(trans);
        }
        trans.setCrossFadeEnabled(true);
        trans.startTransition(400);
        setPadding(pL, pT, pR, pB);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.i("----", motionEvent.getAction() + "");
        if (motionEvent.getAction() == 0) {
            Log.i("----", "Down");
            turnOnImage();
        } else if (motionEvent.getAction() == 1) {
            Log.i("----", "ACTION_UP");
            getBackground().setAlpha(255);
        } else if (motionEvent.getAction() == 3) {
            Log.i("----", "ACTION_CANCEL");
            getBackground().setAlpha(255);
        }
        return false;
    }
}
