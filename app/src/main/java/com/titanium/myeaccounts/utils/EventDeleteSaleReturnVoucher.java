package com.titanium.myeaccounts.utils;

public class EventDeleteSaleReturnVoucher {
    private final String position;

    public EventDeleteSaleReturnVoucher(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}