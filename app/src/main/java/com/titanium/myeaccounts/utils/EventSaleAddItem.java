package com.titanium.myeaccounts.utils;

/**
 * Created by BerylSystems on 11/24/2017.
 */

public class EventSaleAddItem {
    private final String position;

    public EventSaleAddItem(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}
