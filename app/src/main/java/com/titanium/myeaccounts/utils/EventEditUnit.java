package com.titanium.myeaccounts.utils;

public class EventEditUnit {
    private final String position;

    public EventEditUnit(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}