package com.titanium.myeaccounts.utils;

public class EventLongPressBillSundry {
    private final int position;

    public EventLongPressBillSundry(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}