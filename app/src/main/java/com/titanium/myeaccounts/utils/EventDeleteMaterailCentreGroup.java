package com.titanium.myeaccounts.utils;

public class EventDeleteMaterailCentreGroup {
    private final int position;

    public EventDeleteMaterailCentreGroup(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}