package com.titanium.myeaccounts.utils;

public class EventAmountReceivable {

    private final String position;

    public String getPosition() {
        return position;
    }

    public EventAmountReceivable(String position) {
        this.position = position;
    }

}