package com.titanium.myeaccounts.utils;

public class EventDeleteBillSundry {
    private final int position;

    public EventDeleteBillSundry(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}