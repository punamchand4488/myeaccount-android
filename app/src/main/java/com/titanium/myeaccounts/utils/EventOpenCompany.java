package com.titanium.myeaccounts.utils;


public class EventOpenCompany {
    private final int position;

    public EventOpenCompany(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
