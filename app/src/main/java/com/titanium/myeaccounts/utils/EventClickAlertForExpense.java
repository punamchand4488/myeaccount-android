package com.titanium.myeaccounts.utils;

/**
 * Created by BerylSystems on 05-Jan-18.
 */

public class EventClickAlertForExpense {

    private final String position;

    public EventClickAlertForExpense(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}
