package com.titanium.myeaccounts.utils;

public class EventEditMaterialCentre {
    private final String position;

    public EventEditMaterialCentre(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}