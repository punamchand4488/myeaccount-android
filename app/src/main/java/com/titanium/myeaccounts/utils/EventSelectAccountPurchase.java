package com.titanium.myeaccounts.utils;

public class EventSelectAccountPurchase {
    String position;

    public EventSelectAccountPurchase(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}