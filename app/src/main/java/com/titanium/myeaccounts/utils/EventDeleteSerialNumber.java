package com.titanium.myeaccounts.utils;

public class EventDeleteSerialNumber {

    private final String position;

    public EventDeleteSerialNumber(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}