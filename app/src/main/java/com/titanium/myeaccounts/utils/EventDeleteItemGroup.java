package com.titanium.myeaccounts.utils;

public class EventDeleteItemGroup {

    private final int position;

    public EventDeleteItemGroup(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}