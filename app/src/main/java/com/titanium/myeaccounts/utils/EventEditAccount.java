package com.titanium.myeaccounts.utils;

public class EventEditAccount {
    private final String position;

    public EventEditAccount(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}