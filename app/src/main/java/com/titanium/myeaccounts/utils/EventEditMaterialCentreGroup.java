package com.titanium.myeaccounts.utils;

public class EventEditMaterialCentreGroup {
    private final int position;

    public EventEditMaterialCentreGroup(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}