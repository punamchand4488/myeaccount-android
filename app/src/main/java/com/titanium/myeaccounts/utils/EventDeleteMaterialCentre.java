package com.titanium.myeaccounts.utils;

public class EventDeleteMaterialCentre {
    private final String position;

    public EventDeleteMaterialCentre(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}