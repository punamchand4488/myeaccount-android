package com.titanium.myeaccounts.utils;

public class EventDeleteReceiptPdcDetails {

    private final String position;

    public EventDeleteReceiptPdcDetails(String position){
        this.position=position;
    }
    public String getPosition(){
        return position;
    }
}