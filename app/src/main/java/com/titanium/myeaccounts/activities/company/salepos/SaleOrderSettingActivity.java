package com.titanium.myeaccounts.activities.company.salepos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.app.ConnectivityReceiver;
import com.titanium.myeaccounts.activities.company.FirstPageActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.account.ExpandableAccountListActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.item.ExpandableItemListActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.materialcentre.MaterialCentreListActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.saletype.SaleTypeListActivity;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.networks.ApiCallsService;
import com.titanium.myeaccounts.networks.api_response.saleorder.branch.BranchResponse;
import com.titanium.myeaccounts.networks.api_response.saleorder.salesperson.SalesPersonResponse;
import com.titanium.myeaccounts.networks.api_response.saleorder.status.StatusesResponse;
import com.titanium.myeaccounts.networks.api_response.voucherseries.VoucherSeriesResponse;
import com.titanium.myeaccounts.utils.Cv;
import com.titanium.myeaccounts.utils.Helpers;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.ParameterConstant;
import com.titanium.myeaccounts.utils.Preferences;
import com.titanium.myeaccounts.utils.TypefaceCache;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaleOrderSettingActivity extends AppCompatActivity {
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.store)
    TextView mStore;
    @BindView(R.id.spBranch)
    Spinner spBranch;
    @BindView(R.id.spStatus)
    Spinner spStatus;
    @BindView(R.id.spSalesPerson)
    Spinner spSalesPerson;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.submit)
    LinearLayout submit;
    AppUser appUser;
    Animation blinkOnClick;
    ProgressDialog mProgressDialog;
    Snackbar snackbar;
    ArrayList<String> arr_series;
    ArrayAdapter<String> mVoucherAdapter;
    public ArrayList<String> branchList,branchListId, statusList,statusListId, salesPersonList,salesPersonListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_order_setting);
        ButterKnife.bind(this);
        branchList = new ArrayList<>();
        branchListId = new ArrayList<>();
        statusList = new ArrayList<>();
        statusListId = new ArrayList<>();
        salesPersonList = new ArrayList<>();
        salesPersonListId = new ArrayList<>();
        initActionbar();
        FirstPageActivity.posSetting = true;
        FirstPageActivity.posNotifyAdapter = false;
        blinkOnClick = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_on_click);
        appUser = LocalRepositories.getAppUser(getApplicationContext());
        mStore.setText(Preferences.getInstance(getApplicationContext()).getPos_store());
        userName.setText(appUser.cusername);


        Boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            mProgressDialog = new ProgressDialog(SaleOrderSettingActivity.this);
            mProgressDialog.setMessage("Info...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
            ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_BRANCH_LIST);
        } else {
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Boolean isConnected = ConnectivityReceiver.isConnected();
                    if (isConnected) {
                        snackbar.dismiss();
                    }
                }
            });
            snackbar.show();
        }

        mStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUser = LocalRepositories.getAppUser(getApplicationContext());
                ParameterConstant.checkStartActivityResultForAccount = 0;
                ParameterConstant.checkStartActivityResultForMaterialCenter = 1;
                MaterialCentreListActivity.isDirectForMaterialCentre = false;
                startActivityForResult(new Intent(getApplicationContext(), MaterialCentreListActivity.class), 1);
            }
        });

        spBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                   Preferences.getInstance(SaleOrderSettingActivity.this).setBranch_id(branchListId.get(position));
                }else {
                    Preferences.getInstance(SaleOrderSettingActivity.this).setBranch_id("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                   Preferences.getInstance(SaleOrderSettingActivity.this).setStatus_id(statusListId.get(position));
                }else {
                    Preferences.getInstance(SaleOrderSettingActivity.this).setStatus_id("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spSalesPerson.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                   Preferences.getInstance(SaleOrderSettingActivity.this).setSale_person_id(salesPersonListId.get(position));
                }else {
                    Preferences.getInstance(SaleOrderSettingActivity.this).setSale_person_id("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mStore.getText().toString().equals("")) {
                    if (spBranch.getSelectedItemPosition()!=0) {
                       // if (spStatus.getSelectedItemPosition()!=0) {
                            if (spSalesPerson.getSelectedItemPosition()!=0) {
                              /*  Preferences.getInstance(SaleOrderSettingActivity.this).setBranch_id();
                                Preferences.getInstance(SaleOrderSettingActivity.this).setStatus_id();
                                Preferences.getInstance(SaleOrderSettingActivity.this).setSale_person_id();*/

                                finish();
                                //  }
                            } else {
                                Snackbar.make(coordinatorLayout, "Please select sales person ", Snackbar.LENGTH_LONG).show();
                            }
                       /* } else {
                            Snackbar.make(coordinatorLayout, "Please select status ", Snackbar.LENGTH_LONG).show();
                        }*/
                    } else {
                        Snackbar.make(coordinatorLayout, "Please select branch ", Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(coordinatorLayout, "Please select store ", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initActionbar() {
        ActionBar actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar_tittle_text_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#067bc9")));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(viewActionBar, params);
        TextView actionbarTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
        actionbarTitle.setText("POS SETTING");
        actionbarTitle.setTypeface(TypefaceCache.get(getAssets(), 3));
        actionbarTitle.setTextSize(16);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        appUser = LocalRepositories.getAppUser(getApplicationContext());
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("name");
                String id = data.getStringExtra("id");
                String[] name = result.split(",");
                mStore.setText(name[0]);
                Preferences.getInstance(getApplicationContext()).setPos_store(name[0]);
                Preferences.getInstance(getApplicationContext()).setPos_store_id(id);
                return;
            }
        }
    }

    @Subscribe
    public void getBranchList(BranchResponse response) {
        ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_STATUS_LIST);
        ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_SALES_PERSON_LIST);
        int groupindex = -1;
        String group_type = Preferences.getInstance(getApplicationContext()).getBranch_id();
        branchList.add("Select Branch");
        branchListId.add("0");
        for (int i = 0; i < response.getBranches().getData().size(); i++) {
            branchList.add(response.getBranches().getData().get(i).getAttributes().getName());
            branchListId.add(response.getBranches().getData().get(i).getAttributes().getId());
            if (!group_type.equalsIgnoreCase("")){
                if (response.getBranches().getData().get(i).getAttributes().getId().equals(group_type)) {
                    groupindex = i;
                }
            }
        }
        mVoucherAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, branchList);
        mVoucherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBranch.setAdapter(mVoucherAdapter);
        if (groupindex!=-1){
            spBranch.setSelection(groupindex+1);
        }
    }

    @Subscribe
    public void getStatusList(StatusesResponse response) {
        int groupindex = -1;
        String group_type = Preferences.getInstance(getApplicationContext()).getStatus_id();
        statusList.add("Select Status");
        statusListId.add("0");
        for (int i = 0; i < response.getStatuses().getData().size(); i++) {
            statusList.add(response.getStatuses().getData().get(i).getAttributes().getName());
            statusListId.add(response.getStatuses().getData().get(i).getAttributes().getId());
            if (!group_type.equalsIgnoreCase("")){
                if (response.getStatuses().getData().get(i).getAttributes().getId().equals(group_type)) {
                    groupindex = i;
                }
            }
        }
        mVoucherAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, statusList);
        mVoucherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(mVoucherAdapter);
        if (groupindex!=-1){
            spStatus.setSelection(groupindex+1);
        }
    }

    @Subscribe
    public void getSalesPersonList(SalesPersonResponse response) {
        mProgressDialog.dismiss();
        int groupindex = -1;
        String group_type = Preferences.getInstance(getApplicationContext()).getSale_person_id();
        salesPersonList.add("Select Sale Person");
        salesPersonListId.add("0");
        for (int i = 0; i < response.getStatuses().getData().size(); i++) {
            salesPersonList.add(response.getStatuses().getData().get(i).getAttributes().getName());
            salesPersonListId.add(response.getStatuses().getData().get(i).getAttributes().getId());
            if (!group_type.equalsIgnoreCase("")){
                if (response.getStatuses().getData().get(i).getAttributes().getId().equals(group_type)) {
                    groupindex = i;
                }
            }
        }
        mVoucherAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, salesPersonList);
        mVoucherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalesPerson.setAdapter(mVoucherAdapter);
        if (groupindex!=-1){
            spSalesPerson.setSelection(groupindex+1);
        }
    }
}