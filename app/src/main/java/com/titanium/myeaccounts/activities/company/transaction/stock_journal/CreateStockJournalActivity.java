package com.titanium.myeaccounts.activities.company.transaction.stock_journal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.company.navigations.dashboard.TransactionDashboardActivity;
import com.titanium.myeaccounts.activities.company.transaction.SaleVouchersItemDetailsListActivity;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.fragments.transaction.sale.AddItemVoucherFragment;
import com.titanium.myeaccounts.fragments.transaction.sale.CreateSaleVoucherFragment;
import com.titanium.myeaccounts.fragments.transaction.stock_journal.AddItemStockJournalFragment;
import com.titanium.myeaccounts.fragments.transaction.stock_journal.CreateStockJournalFragment;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.TypefaceCache;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateStockJournalActivity extends AppCompatActivity {

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    // public static CompanyData data;
    @BindView(R.id.viewpager)
    ViewPager mHeaderViewPager;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    ProgressDialog mProgressDialog;
    Snackbar snackbar;
    AppUser appUser;
    String title;
    public static boolean fromsalelist;
    public static boolean fromdashboard;
    public static boolean isForEdit;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_stock_journal);
        ButterKnife.bind(this);

        fromdashboard = getIntent().getExtras().getBoolean("fromdashboard");
        fromsalelist = getIntent().getExtras().getBoolean("fromsalelist");
        appUser = LocalRepositories.getAppUser(this);

        if (fromsalelist) {
            if (fromdashboard) {
                title = "CREATE STOCK JOURNAL";
            } else {
                title = "EDIT STOCK JOURNAL";
            }
        } else {
            title = "CREATE STOCK JOURNAL";
        }
        if (isForEdit){
            title = "EDIT STOCK JOURNAL";
        }

        initActionbar();

        setupViewPager(mHeaderViewPager);
        mTabLayout.setupWithViewPager(mHeaderViewPager);
        Intent intent = getIntent();
        boolean b = intent.getBooleanExtra("is", false);
        if (b) {
            mHeaderViewPager.setCurrentItem(1, true);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.list_button);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDefaultDisplayHomeAsUpEnabled(true);


    }

    private void setupViewPager(ViewPager viewPager) {
        CreateStockJournalActivity.ViewPagerAdapter adapter = new CreateStockJournalActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CreateStockJournalFragment(), "CREATE STOCK JOURNAL");
        adapter.addFragment(new AddItemStockJournalFragment(), "ADD ITEM STOCK");
        viewPager.setAdapter(adapter);
    }

    private void initActionbar() {
        ActionBar actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar_tittle_text_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#067bc9")));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(viewActionBar, params);
        TextView actionbarTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);

        actionbarTitle.setText(title);

        actionbarTitle.setTextSize(16);
        actionbarTitle.setTypeface(TypefaceCache.get(getAssets(), 3));
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    public static void hideKeyPad(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       if (!SaleVouchersItemDetailsListActivity.isFromTransactionSaleActivity){
           MenuInflater menuInflater = getMenuInflater();
           menuInflater.inflate(R.menu.activity_list_button_action, menu);
       }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.icon_id:
                Intent i = new Intent(getApplicationContext(), GetStockJournalListActivity.class);
                SaleVouchersItemDetailsListActivity.isFromTransactionSaleActivity = false;
                startActivity(i);
                finish();
                return true;
            case android.R.id.home:
                if (SaleVouchersItemDetailsListActivity.isFromTransactionSaleActivity){
                    Intent intent = new Intent(this, SaleVouchersItemDetailsListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }else {
                    if (fromsalelist){
                        finish();
                    }else {
                        Intent intent = new Intent(this, TransactionDashboardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (SaleVouchersItemDetailsListActivity.isFromTransactionSaleActivity){
            Intent intent = new Intent(this, SaleVouchersItemDetailsListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }else {
            if (fromsalelist){
                finish();
            }else {
                Intent intent = new Intent(this, TransactionDashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }


}
