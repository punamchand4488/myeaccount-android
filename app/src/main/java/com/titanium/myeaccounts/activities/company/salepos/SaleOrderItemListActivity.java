package com.titanium.myeaccounts.activities.company.salepos;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.app.ConnectivityReceiver;
import com.titanium.myeaccounts.activities.company.FirstPageActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.account.ExpandableAccountListActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.item.CreateNewItemActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.materialcentre.MaterialCentreListActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.saletype.SaleTypeListActivity;
import com.titanium.myeaccounts.activities.company.navigations.dashboard.MasterDashboardActivity;
import com.titanium.myeaccounts.activities.company.pos.PosItemAddActivity;
import com.titanium.myeaccounts.activities.company.pos.PosSettingActivity;
import com.titanium.myeaccounts.activities.company.transaction.purchase.CreatePurchaseActivity;
import com.titanium.myeaccounts.activities.company.transaction.purchase.PurchaseAddItemActivity;
import com.titanium.myeaccounts.activities.company.transaction.purchase_return.CreatePurchaseReturnActivity;
import com.titanium.myeaccounts.activities.company.transaction.purchase_return.PurchaseReturnAddItemActivity;
import com.titanium.myeaccounts.activities.company.transaction.sale.CreateSaleActivity;
import com.titanium.myeaccounts.activities.company.transaction.sale.GetSaleVoucherListActivity;
import com.titanium.myeaccounts.activities.company.transaction.sale.SaleVoucherAddItemActivity;
import com.titanium.myeaccounts.activities.company.transaction.sale_return.CreateSaleReturnActivity;
import com.titanium.myeaccounts.activities.company.transaction.sale_return.SaleReturnAddItemActivity;
import com.titanium.myeaccounts.activities.company.transaction.stock_journal.CreateStockJournalActivity;
import com.titanium.myeaccounts.activities.company.transaction.stock_journal.StockJournalAddItemActivity;
import com.titanium.myeaccounts.activities.company.transaction.stocktransfer.CreateStockTransferActivity;
import com.titanium.myeaccounts.activities.company.transaction.stocktransfer.StockTransferAddItemActivity;
import com.titanium.myeaccounts.adapters.ItemExpandableListAdapter;
import com.titanium.myeaccounts.adapters.SaleOrderListAdapter;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.networks.ApiCallsService;
import com.titanium.myeaccounts.networks.api_response.item.DeleteItemResponse;
import com.titanium.myeaccounts.networks.api_response.item.GetItemResponse;
import com.titanium.myeaccounts.networks.api_response.salevoucher.GetSaleVoucherDetails;
import com.titanium.myeaccounts.networks.api_response.voucherseries.VoucherSeriesResponse;
import com.titanium.myeaccounts.utils.Cv;
import com.titanium.myeaccounts.utils.EventDeleteItem;
import com.titanium.myeaccounts.utils.EventEditItem;
import com.titanium.myeaccounts.utils.EventSaleAddItem;
import com.titanium.myeaccounts.utils.Helpers;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.ParameterConstant;
import com.titanium.myeaccounts.utils.Preferences;
import com.titanium.myeaccounts.utils.TypefaceCache;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SaleOrderItemListActivity extends AppCompatActivity {
    //Pos Setting
    @BindView(R.id.date)
    TextView mDate;
    @BindView(R.id.series)
    Spinner mSeries;
    @BindView(R.id.sale_type_layout)
    LinearLayout mSaleTypeLayout;
    @BindView(R.id.sale_type)
    TextView mSaleType;
    @BindView(R.id.party_name)
    TextView mPartyName;
    @BindView(R.id.layoutPartyName)
    LinearLayout layoutPartyName;
    @BindView(R.id.vch_number)
    TextView mVchNumber;
    ArrayAdapter<String> mVoucherAdapter;
    //End pos Setting

    public static Boolean isDirectForItem = true;
    public static Integer comingFrom = 0;
    public static Integer checkForStartActivityResult = 0;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    public static ExpandableListView expListView;
    @BindView(R.id.floating_button)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.autoCompleteTextView)
    AutoCompleteTextView autoCompleteTextView;
    @BindView(R.id.top_layout)
    RelativeLayout mOverlayLayout;
    public TextView mTotal;
    @BindView(R.id.error_layout)
    LinearLayout error_layout;
    @BindView(R.id.pos_setting_layout)
    LinearLayout pos_setting_layout;
    @BindView(R.id.pos_setting)
    ImageView pos_setting;
    @BindView(R.id.submit)
    TextView mSubmit;
    @BindView(R.id.backButton)
    ImageView backButton;
    @BindView(R.id.layoutShippedTo)
    LinearLayout layoutShippedTo;
    @BindView(R.id.tvShipTo)
    TextView tvShipTo;
    @BindView(R.id.submit_layout)
    LinearLayout submit_layout;
    public static SaleOrderListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<Integer, List<String>> listDataChildId;
    HashMap<Integer, List<String>> listDataChildDesc;
    HashMap<Integer, List<String>> listDataChildUnit;
    HashMap<Integer, List<String>> listDataChildAlternateUnit;
    HashMap<Integer, List<String>> listDataChildSalePriceMain;
    HashMap<Integer, List<String>> listDataChildSalePriceAlternate;
    HashMap<Integer, List<Boolean>> listDataChildSerialWise;
    HashMap<Integer, List<Boolean>> listDataChildBatchWise;
    HashMap<Integer, List<String>> listDataChildApplied;
    HashMap<Integer, List<String>> listDataChildAlternateConFactor;
    HashMap<Integer, List<String>> listDataChildDefaultUnit;
    HashMap<Integer, List<String>> listDataChildPackagingConfactor;
    HashMap<Integer, List<String>> listDataChildPackagingSalesPrice;
    HashMap<Integer, List<String>> listDataChildPackagingUnit;
    HashMap<Integer, List<String>> listDataChildMrp;
    HashMap<Integer, List<String>> listDataTax;
    HashMap<Integer, List<String>> listDataBarcode;
    public Map<String, String> mSaleVoucherItem;
    HashMap<Integer, List<String>> listDataChildPurchasePriceMain;
    HashMap<Integer, List<String>> listDataChildPurchasePriceAlternate;
    HashMap<Integer, List<String>> listDataChildPackagingPurchasePrice;
    public Map<String, String> mPurchaseVoucherItem;
    public static HashMap<Integer, String[]> mChildCheckStates;
    public Map<String, String> mPurchaseReturnItem;
    public Map<String, String> mSaleReturnItem;
    public static List<Map<String, String>> mListMapForItemSale;
    public static List<Map<String, String>> mListMapForBillSale;
    public static ArrayList<String> billSundryTotal;
    public static Boolean forSaleType = false;
    // Boolean fromsalelist;


    ProgressDialog mProgressDialog;
    AppUser appUser;
    Snackbar snackbar;
    List<String> name;
    List<String> id;
    List<String> description;
    List<String> unit;
    List<String> alternateUnit;
    List<String> salesPriceMain;
    List<String> salesPriceAlternate;
    List<Boolean> serailWise;
    List<Boolean> batchWise;
    List<String> applied;
    List<String> alternate_con_factor;
    List<String> default_unit;
    List<String> packaging_con_factor;
    List<String> packaging_sales_price;
    List<String> packaging_unit;
    List<String> mrp;
    List<String> tax;
    List<String> purchasePriceMain;
    List<String> purchasePriceAlternate;
    List<String> packaging_purchase_price;
    List<String> barcode;
    List<String> nameList;
    List<String> idList;
    private ArrayAdapter<String> adapter;
    private SimpleDateFormat dateFormatter;
    public static Boolean boolForAdapterSet = false;
    ArrayList<String> mUnitList;
    public static Map mMapPosItem;
    String dateString;
    public static Boolean boolForItemSubmit = false;
    public static Boolean boolForTotal = false;
    public static Map<String, String> transport_details;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_order_item_list);
        ButterKnife.bind(this);
        mListMapForBillSale = new ArrayList();
        billSundryTotal = new ArrayList<>();
        transport_details = new HashMap<>();
        // mListMapPosSaleOrder = new ArrayList<>();
        mListMapForItemSale = new ArrayList<>();
        //initActionbar();
        mChildCheckStates = new HashMap<Integer, String[]>();
        if (FirstPageActivity.pos) {
            SaleOrderItemListActivity.comingFrom = 6;
            SaleOrderItemListActivity.isDirectForItem = false;
            FirstPageActivity.posSetting = false;
            mMapPosItem = new HashMap<>();
        }

        posSettingFun();

        mTotal = (TextView) findViewById(R.id.total);
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        appUser = LocalRepositories.getAppUser(this);
        dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        long date = System.currentTimeMillis();
        dateString = dateFormatter.format(date);
        appUser.stock_in_hand_date = dateString;

        floatingActionButton.setVisibility(View.VISIBLE);
        pos_setting_layout.setVisibility(View.VISIBLE);
        autoCompleteTextView.setVisibility(View.GONE);
        submit_layout.setVisibility(View.VISIBLE);
        submit_layout.bringToFront();
        floatingActionButton.bringToFront();

        appUser.paymentSettlementList = new ArrayList<>();
        appUser.item_name = "";
        appUser.item_code = "";
        appUser.item_hsn_number = "";
        appUser.item_group_name = "";
        appUser.item_unit_name = "";
        appUser.item_tax_category_name = "";
        appUser.item_print_name = "";
        appUser.edit_item_id = "";
        appUser.stock_item_serail_arr.clear();
        appUser.stock_serial_arr.clear();
        Preferences.getInstance(getApplicationContext()).setStockSerial("");
        LocalRepositories.saveAppUser(getApplicationContext(), appUser);

        pos_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SaleOrderSettingActivity.class);
                startActivity(intent);
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(ExpandableItemListActivity.this, ExpandableItemListActivity.mTotal.getText().toString(), Toast.LENGTH_SHORT).show();

                if (!mSeries.getSelectedItem().toString().equals("")) {
                    if (!mDate.getText().toString().equals("")) {
                        if (!mVchNumber.getText().toString().equals("")) {
                            if (!mSaleType.getText().toString().equals("")) {
                                if (!mPartyName.getText().toString().equals("")) {
                                    if (!tvShipTo.getText().toString().equals("")) {
                                        if (!Preferences.getInstance(getApplicationContext()).getPos_store().equals("")) {
                                            if (!Preferences.getInstance(getApplicationContext()).getBranch_id().equals("")) {
                                                if (!Preferences.getInstance(getApplicationContext()).getSale_person_id().equals("")) {
                                                    Preferences.getInstance(getApplicationContext()).setVoucherSeries(mSeries.getSelectedItem().toString());
                                                    Preferences.getInstance(getApplicationContext()).setPos_date(mDate.getText().toString());
                                                    Preferences.getInstance(getApplicationContext()).setVoucher_number(mVchNumber.getText().toString());
                                                    Preferences.getInstance(getApplicationContext()).setPos_sale_type(mSaleType.getText().toString());
                                                    Preferences.getInstance(getApplicationContext()).setPos_party_name(mPartyName.getText().toString());
                                                    Preferences.getInstance(getApplicationContext()).setShipped_to_sale_order(tvShipTo.getText().toString());
                                                    //  if (mMapPosItem.size() > 0) {
                                                    // Preferences.getInstance(getApplicationContext()).setVoucher_number(mVchNumber.getText().toString());
                                                    //  mListMapForItemSale.clear();

                                                    Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                                                            R.anim.blink_on_click);
                                                    v.startAnimation(animFadeIn);
                                                    Preferences.getInstance(getApplicationContext()).setParty_name("");
                                                    Preferences.getInstance(getApplicationContext()).setParty_id("");
                                                    // appUser = LocalRepositories.getAppUser(ExpandableItemListActivity.this);
                                                    if (mListMapForItemSale.size() > 0) {
                                                        //  mListMapForBillSale.clear();
                                                        //  mListMapForItemSale.clear();
                                                        //  mListMapForItemSale = mListMapPosSaleOrder;
                                                        // mMapPosItem.clear();
                                                        boolForItemSubmit = false;
                                                        Double subtotal = 0.0;
                                                        for (int i = 0; i < mListMapForItemSale.size(); i++) {
                                                            Double total = 0.0;
                                                            Map map = mListMapForItemSale.get(i);
                                                            String tax = (String) map.get("tax");
                                                            String itemid = (String) map.get("item_id");
                                                            total = Double.valueOf((String) map.get("tempTotal"));
                                                            if (mSaleType.getText().toString().contains("GST-ItemWise") && tax.contains("GST ")) {
                                                                Double item_tax = (Double.valueOf(total) * taxSplit(tax)) / 100;
                                                                Double taxInclude = Double.valueOf(total) + item_tax;
                                                                total = taxInclude;
                                                                map.put("total", "" + total);
                                                            }
                                                            String multiRate = "0";
                                                            if (mSaleType.getText().toString().contains("GST-MultiRate") && tax.contains("GST ")) {
                                                                multiRate = String.valueOf(taxSplit(tax));
                                                                map.put(itemid, multiRate);
                                                            }
                                                            mListMapForItemSale.set(i, map);
                                                            subtotal = subtotal + total;
                                                        }
                                                        Intent intent = new Intent(getApplicationContext(), SaleOrderItemAddActivity.class);
                                                        intent.putExtra("subtotal", subtotal);
                                                        // boolForAdapterSet = true;
                                                        startActivity(intent);
                                                    } else {
                                                        Toast.makeText(SaleOrderItemListActivity.this, "Please add item!!!", Toast.LENGTH_SHORT).show();
                                                    }
                                           /* if (mListMapForItemSale.size() > 0) {
                                                mMapPosItem.clear();
                                                Intent intent = new Intent(getApplicationContext(), PosItemAddActivity.class);
                                                intent.putExtra("subtotal", subtotal);
                                               // boolForAdapterSet = true;
                                                boolForItemSubmit = false;
                                                startActivity(intent);
                                            } else {
                                                Toast.makeText(ExpandableItemListActivity.this, "Please add item!!!", Toast.LENGTH_SHORT).show();
                                            }*/
                                      /*  } else {
                                            Toast.makeText(ExpandableItemListActivity.this, "Please add item!!!", Toast.LENGTH_SHORT).show();
                                        }*/
                                                } else {
                                                    Toast.makeText(SaleOrderItemListActivity.this, "Please select sales person in setting menu!!!", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(SaleOrderItemListActivity.this, "Please select branch in setting menu!!!", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(SaleOrderItemListActivity.this, "Please select store in setting menu!!!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(SaleOrderItemListActivity.this, "Please ship to!!!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(SaleOrderItemListActivity.this, "Please select party name!!!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(SaleOrderItemListActivity.this, "Please select sale type!!!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(SaleOrderItemListActivity.this, "Please select voucher number!!!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SaleOrderItemListActivity.this, "Please select date!!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SaleOrderItemListActivity.this, "Please select voucher series!!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void posSettingFun() {
        dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        final Calendar newCalendar = Calendar.getInstance();
        String date1 = dateFormatter.format(newCalendar.getTime());
        if (Preferences.getInstance(getApplicationContext()).getPos_date().equals("")) {
            Preferences.getInstance(getApplicationContext()).setPos_date(date1);
        } else {
            mDate.setText(Preferences.getInstance(getApplicationContext()).getPos_date());
        }
        mSaleType.setText(Preferences.getInstance(getApplicationContext()).getPos_sale_type());
        mDate.setText(Preferences.getInstance(getApplicationContext()).getPos_date());
        mPartyName.setText(Preferences.getInstance(getApplicationContext()).getPos_party_name());
        tvShipTo.setText(Preferences.getInstance(getApplicationContext()).getShipped_to_sale_order());

      /*  if (!FirstPageActivity.pos){
            Boolean isConnected = ConnectivityReceiver.isConnected();
            if (isConnected) {
                mProgressDialog = new ProgressDialog(SaleOrderItemListActivity.this);
                mProgressDialog.setMessage("Info...");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
                appUser = LocalRepositories.getAppUser(getApplicationContext());
                appUser.arr_series.clear();
                appUser.series_details.clear();
                LocalRepositories.saveAppUser(getApplicationContext(),appUser);
                ApiCallsService.action(getApplicationContext(), Cv.ACTION_VOUCHER_SERIES);
            } else {
                snackbar = Snackbar
                        .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Boolean isConnected = ConnectivityReceiver.isConnected();
                                if (isConnected) {
                                    snackbar.dismiss();
                                }
                            }
                        });
                snackbar.show();
            }
        }else {
            mSeries.setEnabled(false);
            mVoucherAdapter = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_spinner_item, appUser.arr_series);
            mVoucherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSeries.setAdapter(mVoucherAdapter);
            mSeries.setEnabled(false);
            if (Preferences.getInstance(getApplicationContext()).getAuto_increment().equals("true")) {
                mVchNumber.setEnabled(false);
            } else {
                mVchNumber.setEnabled(true);
            }
            mVchNumber.setText(Preferences.getInstance(getApplicationContext()).getVoucher_number());
        }

        if (Preferences.getInstance(getApplicationContext()).getAuto_increment()!=null){
            if (Preferences.getInstance(getApplicationContext()).getAuto_increment().equals("true")){
                mVchNumber.setEnabled(false);
            }else {
                mVchNumber.setEnabled(true);
            }
        }

        mVoucherAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, appUser.arr_series);
        mVoucherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSeries.setAdapter(mVoucherAdapter);
        String group_type = Preferences.getInstance(getApplicationContext()).getVoucherSeries();
        int groupindex = -1;
        for (int i = 0; i < appUser.arr_series.size(); i++) {
            if (appUser.arr_series.get(i).equals(group_type)) {
                groupindex = i;
                break;
            }
        }
        mSeries.setSelection(groupindex);*/
        mSeries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map map = new HashMap();
                if (appUser.series_details.size() > 0) {
                    map = appUser.series_details.get(position);
                    String auto_increament = (String) map.get("auto_increment");
                    String vch_number = (String) map.get("voucher_number");
                    if (auto_increament.equals("false")) {
                        mVchNumber.setEnabled(true);
                    } else {
                        mVchNumber.setEnabled(false);
                    }
                    mVchNumber.setText(vch_number);
                    if (Preferences.getInstance(getApplicationContext()).getVoucherSeries().equals(mSeries.getSelectedItem().toString())) {
                        mVchNumber.setText(Preferences.getInstance(getApplicationContext()).getVoucher_number());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUser = LocalRepositories.getAppUser(getApplicationContext());
                DatePickerDialog datePickerDialog = new DatePickerDialog(SaleOrderItemListActivity.this, new android.app.DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        String date = dateFormatter.format(newDate.getTime());
                        mDate.setText(date);
                        Preferences.getInstance(getApplicationContext()).setPos_date(date);
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        mSaleType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUser = LocalRepositories.getAppUser(getApplicationContext());
                ParameterConstant.checkStartActivityResultForAccount = 0;
                SaleTypeListActivity.isDirectForSaleType = false;
                startActivityForResult(new Intent(getApplicationContext(), SaleTypeListActivity.class), 2);
            }
        });
        layoutPartyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUser = LocalRepositories.getAppUser(getApplicationContext());
                ParameterConstant.forAccountIntentBool = false;
                appUser.account_master_group = "Sundry Debtors,Sundry Creditors,Cash-in-hand";
                ExpandableAccountListActivity.isDirectForAccount = false;
                LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                ParameterConstant.handleAutoCompleteTextView = 0;
                Intent intent = new Intent(getApplicationContext(), ExpandableAccountListActivity.class);
                //intent.putExtra("bool",true);
                startActivityForResult(intent, 3);
            }
        });

        layoutShippedTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUser = LocalRepositories.getAppUser(SaleOrderItemListActivity.this);
                ParameterConstant.forAccountIntentBool = false;
                ParameterConstant.forAccountIntentName = "";
                ParameterConstant.forAccountIntentId = "";
                ParameterConstant.forAccountIntentMobile = "";
                //ParameterConstant.checkStartActivityResultForAccount = 0;
                appUser.account_master_group = "Sundry Debtors,Sundry Creditors";
                ExpandableAccountListActivity.isDirectForAccount = false;
                LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                ParameterConstant.handleAutoCompleteTextView = 0;
                Intent intent = new Intent(SaleOrderItemListActivity.this, ExpandableAccountListActivity.class);
                //intent.putExtra("bool",true);
                startActivityForResult(intent, 1);
            }
        });
    }

    private boolean isFirstTime() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        //SharedPreferences preferences1=getP
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
            mOverlayLayout.setVisibility(View.VISIBLE);
            mOverlayLayout.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mOverlayLayout.setVisibility(View.INVISIBLE);
                    return false;
                }
            });
        }
        return ranBefore;
    }


    private void initActionbar() {
        ActionBar actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar_tittle_text_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#067bc9")));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(viewActionBar, params);
        TextView actionbarTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
        actionbarTitle.setText("ITEM LIST");
        actionbarTitle.setTypeface(TypefaceCache.get(getAssets(), 3));
        actionbarTitle.setTextSize(16);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    protected void onResume() {
        //   mChildCheckStates = new HashMap<Integer, String[]>();
        if (Preferences.getInstance(getApplicationContext()).getPos_date().equals("")) {
            Preferences.getInstance(getApplicationContext()).setPos_date(dateString);
        }
        super.onResume();
        mSaleVoucherItem = new HashMap<>();
        mPurchaseVoucherItem = new HashMap<>();
        mPurchaseReturnItem = new HashMap<>();
        mSaleReturnItem = new HashMap<>();
        EventBus.getDefault().register(this);
        appUser.item_unit_name = "";
        LocalRepositories.saveAppUser(getApplicationContext(), appUser);
        Boolean isConnected = ConnectivityReceiver.isConnected();
        /* if(isDirectForItem==true){*/
        if (!FirstPageActivity.posSetting) {
            mListMapForItemSale = new ArrayList();
            FirstPageActivity.posNotifyAdapter = false;
            if (isConnected) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Info...");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
                //SaleOrderListAdapter.mMapPosItem.clear();
                ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_ITEM);
            } else {
                snackbar = Snackbar
                        .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Boolean isConnected = ConnectivityReceiver.isConnected();
                                if (isConnected) {
                                    snackbar.dismiss();
                                }
                            }
                        });
                snackbar.show();
            }
        }

        Double total = 0.0;
        for (int i = 0; i < mListMapForItemSale.size(); i++) {
            total = total + Double.valueOf(mListMapForItemSale.get(i).get("total"));
        }
        mTotal.setText("Total : " + "" + total);

        if (FirstPageActivity.posNotifyAdapter) {
            mChildCheckStates = new HashMap<Integer, String[]>();
            expListView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
            for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                expListView.expandGroup(i);
            }
        }
 /*       }else{
            if (isConnected) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Info...");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(true);
                mProgressDialog.show();
                LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_ITEM_MATERRIAL_CENTRE);
            } else {
                snackbar = Snackbar
                        .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Boolean isConnected = ConnectivityReceiver.isConnected();
                                if (isConnected) {
                                    snackbar.dismiss();
                                }
                            }
                        });
                snackbar.show();
            }
        }*/
    }

    @Override
    protected void onPause() {
        FirstPageActivity.posSetting = true;
        FirstPageActivity.posNotifyAdapter = false;
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void add(View v) {

        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink_on_click);
        v.startAnimation(animFadeIn);
        Intent intent = new Intent(getApplicationContext(), CreateNewItemActivity.class);
        FirstPageActivity.boolForSaleOrder = true;
        intent.putExtra("fromitemlist", false);
        startActivity(intent);
    }


    @Subscribe
    public void getItem(GetItemResponse response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {
            mTotal.setText("Total : 0.0");
            nameList = new ArrayList();
            idList = new ArrayList();
            nameList = new ArrayList();

            listDataHeader = new ArrayList<>();
            listDataChildDesc = new HashMap<Integer, List<String>>();
            listDataChildUnit = new HashMap<Integer, List<String>>();
            listDataChildAlternateUnit = new HashMap<Integer, List<String>>();
            listDataChildSalePriceMain = new HashMap<Integer, List<String>>();
            listDataChildPurchasePriceMain = new HashMap<Integer, List<String>>();
            listDataBarcode = new HashMap<Integer, List<String>>();

            listDataChildSalePriceAlternate = new HashMap<Integer, List<String>>();
            listDataChildPurchasePriceAlternate = new HashMap<Integer, List<String>>();

            listDataChildSerialWise = new HashMap<Integer, List<Boolean>>();
            listDataChildBatchWise = new HashMap<Integer, List<Boolean>>();
            listDataChildApplied = new HashMap<Integer, List<String>>();
            listDataChildAlternateConFactor = new HashMap<Integer, List<String>>();
            listDataChildDefaultUnit = new HashMap<Integer, List<String>>();
            listDataChildPackagingUnit = new HashMap<Integer, List<String>>();
            listDataChildDefaultUnit = new HashMap<Integer, List<String>>();
            listDataChildPackagingSalesPrice = new HashMap<Integer, List<String>>();
            listDataChildPackagingPurchasePrice = new HashMap<Integer, List<String>>();
            listDataChildPackagingConfactor = new HashMap<Integer, List<String>>();
            listDataChild = new HashMap<String, List<String>>();
            listDataChildId = new HashMap<Integer, List<String>>();
            listDataChildMrp = new HashMap<Integer, List<String>>();
            listDataTax = new HashMap<Integer, List<String>>();
            if (response.getOrdered_items().size() == 0) {
                expListView.setVisibility(View.GONE);
                error_layout.setVisibility(View.VISIBLE);
            } else {
                expListView.setVisibility(View.VISIBLE);
                error_layout.setVisibility(View.GONE);
            }
            for (int i = 0; i < response.getOrdered_items().size(); i++) {
                listDataHeader.add(response.getOrdered_items().get(i).getGroup_name());
                name = new ArrayList<>();
                description = new ArrayList<>();
                unit = new ArrayList<>();
                salesPriceMain = new ArrayList<>();
                purchasePriceMain = new ArrayList<>();
                salesPriceAlternate = new ArrayList<>();
                purchasePriceAlternate = new ArrayList<>();
                alternateUnit = new ArrayList<>();
                serailWise = new ArrayList<>();
                applied = new ArrayList<>();
                batchWise = new ArrayList<>();
                alternate_con_factor = new ArrayList<>();
                default_unit = new ArrayList<>();
                packaging_con_factor = new ArrayList<>();
                packaging_sales_price = new ArrayList<>();
                packaging_purchase_price = new ArrayList<>();
                packaging_unit = new ArrayList<>();
                mrp = new ArrayList<>();
                tax = new ArrayList<>();
                barcode = new ArrayList<>();
                id = new ArrayList<>();
                default_unit.clear();
                for (int j = 0; j < response.getOrdered_items().get(i).getData().size(); j++) {
                    name.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getName()
                            + "," + String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getTotal_stock_quantity()
                            + "," + response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_main()
                            + "," + response.getOrdered_items().get(i).getData().get(j).getId()
                    +","+response.getOrdered_items().get(i).getData().get(j).getAttributes().getTax_category()));

                    nameList.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getName());
                    idList.add(String.valueOf(i) + "," + String.valueOf(j));

                    if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getItem_description() != null) {
                        description.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getItem_description());
                    } else {
                        description.add("");
                    }
                    if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_applied_on() != null) {
                        applied.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_applied_on());
                    } else {
                        applied.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_main()) != null) {
                        salesPriceMain.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_main()));
                    } else {
                        salesPriceMain.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getPurchase_price_main()) != null) {
                        purchasePriceMain.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getPurchase_price_main()));
                    } else {
                        purchasePriceMain.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_alternate()) != null) {
                        salesPriceAlternate.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSales_price_alternate()));
                    } else {
                        salesPriceAlternate.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getPurchase_price_alternate()) != null) {
                        purchasePriceAlternate.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getPurchase_price_alternate()));
                    } else {
                        purchasePriceAlternate.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getConversion_factor()) != null) {
                        alternate_con_factor.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getConversion_factor()));
                    } else {
                        alternate_con_factor.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getMrp()) != null) {
                        mrp.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getMrp()));
                    } else {
                        mrp.add("");
                    }
                    StringBuilder sb = new StringBuilder();
                    if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getBarcode() != null) {
                        for (String str : response.getOrdered_items().get(i).getData().get(j).getAttributes().getBarcode()) {
                            sb.append(str).append(","); //separating contents using semi colon
                        }
                        String strfromArrayList = sb.toString();

                      /*  for(int k=0;k<response.getOrdered_items().get(i).getData().get(j).getAttributes().getBarcode().size();k++){
                            String barcode= StringUtils.collectionToDelimitedString(",",response.getOrdered_items().get(i).getData().get(j).getAttributes().getBarcode().get(k).toString());

                        }*/
                        barcode.add(strfromArrayList);

                        Timber.i("MYBARCODE" + barcode);
                    } else {
                        barcode.add("");

                    }
                    unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getItem_unit());

                    if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getAlternate_unit() != null) {
                        alternateUnit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getAlternate_unit());
                    } else {
                        alternateUnit.add("");
                    }
                    if (SaleOrderItemListActivity.comingFrom == 0) {
                        if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_sales() != null) {
                            default_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_sales());
                        } else {
                            default_unit.add("");
                        }
                    } else if (SaleOrderItemListActivity.comingFrom == 14 || SaleOrderItemListActivity.comingFrom == 15) {
                        if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_sales() != null) {
                            default_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_sales());
                        } else {
                            default_unit.add("");
                        }
                    } else if (SaleOrderItemListActivity.comingFrom == 1) {
                        if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_purchase() != null) {
                            default_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_purchase());
                        } else {
                            default_unit.add("");
                        }
                    } else if (SaleOrderItemListActivity.comingFrom == 2) {
                        if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_sales() != null) {
                            default_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_sales());
                        } else {
                            default_unit.add("");
                        }
                    } else if (SaleOrderItemListActivity.comingFrom == 3) {
                        if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_purchase() != null) {
                            default_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_purchase());
                        } else {
                            default_unit.add("");
                        }
                    } else if (SaleOrderItemListActivity.comingFrom == 4) {
                        if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_purchase() != null) {
                            default_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getDefault_unit_for_purchase());
                        } else {
                            default_unit.add("");
                        }
                    }

                    if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getItem_package_unit() != null) {
                        packaging_unit.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getItem_package_unit());
                    } else {
                        packaging_unit.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getConversion_factor_package()) != null) {
                        packaging_con_factor.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getConversion_factor_package()));
                    } else {
                        packaging_con_factor.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSale_price()) != null) {
                        packaging_sales_price.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getSale_price()));
                    } else {
                        packaging_sales_price.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getPurchase_price()) != null) {
                        packaging_purchase_price.add(String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().getPurchase_price()));
                    } else {
                        packaging_purchase_price.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().isBatch_wise_detail()) != null) {
                        batchWise.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().isBatch_wise_detail());
                    } else {
                        batchWise.add(false);
                    }
                    if (response.getOrdered_items().get(i).getData().get(j).getAttributes().getTax_category() != null) {
                        tax.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().getTax_category());
                    } else {
                        tax.add("");
                    }
                    if (String.valueOf(response.getOrdered_items().get(i).getData().get(j).getAttributes().isSerial_number_wise_detail()) != null) {
                        serailWise.add(response.getOrdered_items().get(i).getData().get(j).getAttributes().isSerial_number_wise_detail());
                    } else {
                        serailWise.add(false);
                    }
                    id.add(response.getOrdered_items().get(i).getData().get(j).getId());
                }
                listDataChild.put(listDataHeader.get(i), name);
                listDataChildId.put(i, id);
                listDataChildDesc.put(i, description);
                listDataChildUnit.put(i, unit);
                listDataChildAlternateUnit.put(i, alternateUnit);
                listDataChildSalePriceMain.put(i, salesPriceMain);
                listDataChildPurchasePriceMain.put(i, purchasePriceMain);

                listDataChildSalePriceAlternate.put(i, salesPriceAlternate);
                listDataChildPurchasePriceAlternate.put(i, purchasePriceAlternate);

                listDataChildSerialWise.put(i, serailWise);
                listDataChildBatchWise.put(i, batchWise);
                listDataChildApplied.put(i, applied);
                listDataChildAlternateConFactor.put(i, alternate_con_factor);
                listDataChildDefaultUnit.put(i, default_unit);
                listDataChildPackagingConfactor.put(i, packaging_con_factor);
                listDataChildPackagingSalesPrice.put(i, packaging_sales_price);
                listDataChildPackagingPurchasePrice.put(i, packaging_purchase_price);

                listDataChildPackagingUnit.put(i, packaging_unit);
                listDataChildMrp.put(i, mrp);
                listDataTax.put(i, tax);
                listDataBarcode.put(i, barcode);
            }
            listAdapter = new SaleOrderListAdapter(this, listDataHeader, listDataChild, listDataChildSalePriceMain);

            // setting list adapter
            expListView.setAdapter(listAdapter);
            if (FirstPageActivity.pos) {
                for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                    expListView.expandGroup(i);
                }
                posVoucherDetails();
               /* if (!ExpandableItemListActivity.forSaleType){
                    posVoucherDetails();
                }else {
                    mProgressDialog.dismiss();
                }*/
            } else {
                for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                    expListView.expandGroup(i);
                }
                ApiCallsService.action(getApplicationContext(), Cv.ACTION_VOUCHER_SERIES);
                   /* if (!ExpandableItemListActivity.forSaleType){
                        ApiCallsService.action(getApplicationContext(), Cv.ACTION_VOUCHER_SERIES);
                    }else {
                        mProgressDialog.dismiss();
                    }*/
            }

            autoCompleteTextView();

        } else {
            //   startActivity(new Intent(getApplicationContext(), MasterDashboardActivity.class));
            // Snackbar.make(coordinatorLayout, response.getMessage(), Snackbar.LENGTH_LONG).show();
            Helpers.dialogMessage(this, response.getMessage());
        }
    }


    @Subscribe
    public void delete_item(EventDeleteItem pos) {
        String id = pos.getPosition();
        String[] arr = id.split(",");
        String groupid = arr[0];
        String childid = arr[1];
        String arrid = listDataChildId.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
        appUser.delete_item_id = arrid;
        LocalRepositories.saveAppUser(this, appUser);
        new AlertDialog.Builder(SaleOrderItemListActivity.this)
                .setTitle("Delete Item")
                .setMessage("Are you sure you want to delete this item ?")
                .setPositiveButton(R.string.btn_ok, (dialogInterface, i) -> {
                    Boolean isConnected = ConnectivityReceiver.isConnected();
                    if (isConnected) {
                        mProgressDialog = new ProgressDialog(SaleOrderItemListActivity.this);
                        mProgressDialog.setMessage("Info...");
                        mProgressDialog.setIndeterminate(false);
                        mProgressDialog.setCancelable(true);
                        mProgressDialog.show();
                        ApiCallsService.action(getApplicationContext(), Cv.ACTION_DELETE_ITEM);
                    } else {
                        snackbar = Snackbar
                                .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Boolean isConnected = ConnectivityReceiver.isConnected();
                                        if (isConnected) {
                                            snackbar.dismiss();
                                        }
                                    }
                                });
                        snackbar.show();
                    }

                })
                .setNegativeButton(R.string.btn_cancel, null)
                .show();

    }

    @Subscribe
    public void deleteitemresponse(DeleteItemResponse response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {
            ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_ITEM);
            Snackbar
                    .make(coordinatorLayout, response.getMessage(), Snackbar.LENGTH_LONG).show();
        } else {
            //Snackbar.make(coordinatorLayout, response.getMessage(), Snackbar.LENGTH_LONG).show();
            Helpers.dialogMessage(this, response.getMessage());
        }
    }

    @Subscribe
    public void editgroup(EventEditItem pos) {
        String id = pos.getPosition();
        String[] arr = id.split(",");
        String groupid = arr[0];
        String childid = arr[1];
        String arrid = listDataChildId.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
        appUser.edit_item_id = arrid;
        LocalRepositories.saveAppUser(this, appUser);
        Intent intent = new Intent(getApplicationContext(), CreateNewItemActivity.class);
        intent.putExtra("fromitemlist", true);
        startActivity(intent);
    }

    @Subscribe
    public void ClickEventAddSaleVoucher(EventSaleAddItem pos) {


        autoCompleteTextView();

        String id = pos.getPosition();
        String[] arr = id.split(",");
        String groupid = arr[0];
        String childid = arr[1];
        String arrid = listDataChildId.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
        appUser.childId = arrid;
        LocalRepositories.saveAppUser(this, appUser);

        int itemPos = -1;
        for (int i = 0; i < SaleOrderItemListActivity.mListMapForItemSale.size(); i++) {
            if (arrid.equals(SaleOrderItemListActivity.mListMapForItemSale.get(i).get("item_id"))) {
                itemPos = i;
            }
        }
        if (itemPos != -1) {
            Intent intent = new Intent(getApplicationContext(), SalePosAddItemActivity.class);
            intent.putExtra("frombillitemvoucherlist", true);
            intent.putExtra("bool", true);
            intent.putExtra("pos", itemPos);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), SalePosAddItemActivity.class);
            String itemid = listDataChildId.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String itemName = listDataChild.get(listDataHeader.get(Integer.parseInt(groupid))).get(Integer.parseInt(childid));
            String arr1[] = itemName.split(",");
            itemName = arr1[0];
            String descr;
            String alternate_unit;
            String sales_price_main;
            String sales_price_alternate;
            Boolean batch, serial;
            descr = listDataChildDesc.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            sales_price_main = listDataChildSalePriceMain.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            sales_price_alternate = listDataChildSalePriceAlternate.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            alternate_unit = listDataChildAlternateUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            batch = listDataChildBatchWise.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            serial = listDataChildSerialWise.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String main_unit = listDataChildUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String applied = listDataChildApplied.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String alternate_unit_con_factor = listDataChildAlternateConFactor.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            // String default_unit = listDataChildDefaultUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String packaging_unit_con_factor = listDataChildPackagingConfactor.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String packaging_unit_sales_price = listDataChildPackagingSalesPrice.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String packaging_unit = listDataChildPackagingUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String mrp = listDataChildMrp.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String tax = listDataTax.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            String barcode = listDataBarcode.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
            intent.putExtra("fromitemlist", true);
            intent.putExtra("fromSaleVoucherItemList", true);
            mSaleVoucherItem.put("id", "");
            mSaleVoucherItem.put("item_id", itemid);
            String multiRate = "0";
            mSaleVoucherItem.put("name", itemName);
            mSaleVoucherItem.put("desc", descr);
            mSaleVoucherItem.put("main_unit", main_unit);
            mSaleVoucherItem.put("alternate_unit", alternate_unit);
            mSaleVoucherItem.put("serial_wise", String.valueOf(serial));
            mSaleVoucherItem.put("batch_wise", String.valueOf(batch));
            mSaleVoucherItem.put("sales_price_main", sales_price_main);
            mSaleVoucherItem.put("applied", applied);
            mSaleVoucherItem.put("alternate_unit_con_factor", alternate_unit_con_factor);
            mSaleVoucherItem.put("default_unit", "");
            mSaleVoucherItem.put("packaging_unit_con_factor", packaging_unit_con_factor);
            mSaleVoucherItem.put("packaging_unit_sales_price", packaging_unit_sales_price);
            mSaleVoucherItem.put("packaging_unit", packaging_unit);
            mSaleVoucherItem.put("mrp", mrp);
            mSaleVoucherItem.put("tax", tax);
          /*  appUser.mMapSaleVoucherItem = mSaleVoucherItem;
            LocalRepositories.saveAppUser(this, appUser);*/
            intent.putExtra("item_id", itemid);
            intent.putExtra("name", itemName);
            intent.putExtra(itemid, multiRate);
            intent.putExtra("desc", descr);
            intent.putExtra("main_unit", main_unit);
            intent.putExtra("alternate_unit", alternate_unit);
            intent.putExtra("serial_wise", serial);
            intent.putExtra("batch_wise", batch);
            intent.putExtra("sales_price_main", sales_price_main);
            intent.putExtra("sales_price_alternate", sales_price_alternate);
            intent.putExtra("applied", applied);
            intent.putExtra("alternate_unit_con_factor", alternate_unit_con_factor);
            intent.putExtra("default_unit", "");
            intent.putExtra("packaging_unit_con_factor", packaging_unit_con_factor);
            intent.putExtra("packaging_unit_sales_price", packaging_unit_sales_price);
            intent.putExtra("packaging_unit", packaging_unit);
            intent.putExtra("mrp", mrp);
            intent.putExtra("tax", tax);
            intent.putExtra("barcode", barcode);
            intent.putExtra("frombillitemvoucherlist", false);
            intent.putExtra("fromPos", true);
              /*  if(fromsalelist){
                    intent.putExtra("fromsalelist", true);
                }
                else{
                    intent.putExtra("fromsalelist", false);
                }*/
            startActivity(intent);
            //finish();
        }
    }

    @Override
    public void onBackPressed() {
       /* Intent intent = new Intent(this, FirstPageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);*/
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void autoCompleteTextView() {
        autoCompleteTextView.setThreshold(1);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, nameList);
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                autoCompleteTextView.setText("");
                String id = idList.get(getPositionOfItem(adapter.getItem(i)));
                Timber.i("IDD----" + id);
                String[] arr = id.split(",");
                String groupid = arr[0];
                String childid = arr[1];
                String arrid = listDataChildId.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
               /* appUser.childId = arrid;
                LocalRepositories.saveAppUser(this, appUser);
*/

                Intent intent = new Intent(getApplicationContext(), SaleOrderItemListActivity.class);
                String itemid = listDataChildId.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String itemName = listDataChild.get(listDataHeader.get(Integer.parseInt(groupid))).get(Integer.parseInt(childid));
                String arr1[] = itemName.split(",");
                itemName = arr1[0];
                String descr;
                String alternate_unit;
                String sales_price_main;
                String sales_price_alternate;
                Boolean batch, serial;
                descr = listDataChildDesc.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                sales_price_main = listDataChildSalePriceMain.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                sales_price_alternate = listDataChildSalePriceAlternate.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                alternate_unit = listDataChildAlternateUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                batch = listDataChildBatchWise.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                serial = listDataChildSerialWise.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String main_unit = listDataChildUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String applied = listDataChildApplied.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String alternate_unit_con_factor = listDataChildAlternateConFactor.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String default_unit = listDataChildDefaultUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String packaging_unit_con_factor = listDataChildPackagingConfactor.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String packaging_unit_sales_price = listDataChildPackagingSalesPrice.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String packaging_unit = listDataChildPackagingUnit.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String mrp = listDataChildMrp.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String tax = listDataTax.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                String barcode = listDataBarcode.get(Integer.parseInt(groupid)).get(Integer.parseInt(childid));
                intent.putExtra("fromitemlist", true);
                intent.putExtra("fromSaleVoucherItemList", true);
                mSaleVoucherItem.put("name", itemName);
                mSaleVoucherItem.put("desc", descr);
                mSaleVoucherItem.put("main_unit", main_unit);
                mSaleVoucherItem.put("alternate_unit", alternate_unit);
                mSaleVoucherItem.put("serial_wise", String.valueOf(serial));
                mSaleVoucherItem.put("batch_wise", String.valueOf(batch));
                mSaleVoucherItem.put("sales_price_main", sales_price_main);
                mSaleVoucherItem.put("applied", applied);
                mSaleVoucherItem.put("alternate_unit_con_factor", alternate_unit_con_factor);
                mSaleVoucherItem.put("default_unit", default_unit);
                mSaleVoucherItem.put("packaging_unit_con_factor", packaging_unit_con_factor);
                mSaleVoucherItem.put("packaging_unit_sales_price", packaging_unit_sales_price);
                mSaleVoucherItem.put("packaging_unit", packaging_unit);
                mSaleVoucherItem.put("mrp", mrp);
                mSaleVoucherItem.put("tax", tax);
                appUser.mMapSaleVoucherItem = mSaleVoucherItem;
                LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                intent.putExtra("item_id", itemid);
                intent.putExtra("name", itemName);
                intent.putExtra("desc", descr);
                intent.putExtra("main_unit", main_unit);
                intent.putExtra("alternate_unit", alternate_unit);
                intent.putExtra("serial_wise", serial);
                intent.putExtra("batch_wise", batch);
                intent.putExtra("sales_price_main", sales_price_main);
                intent.putExtra("sales_price_alternate", sales_price_alternate);
                intent.putExtra("applied", applied);
                intent.putExtra("alternate_unit_con_factor", alternate_unit_con_factor);
                intent.putExtra("default_unit", default_unit);
                intent.putExtra("packaging_unit_con_factor", packaging_unit_con_factor);
                intent.putExtra("packaging_unit_sales_price", packaging_unit_sales_price);
                intent.putExtra("packaging_unit", packaging_unit);
                intent.putExtra("mrp", mrp);
                intent.putExtra("tax", tax);
                intent.putExtra("barcode", barcode);
                intent.putExtra("frombillitemvoucherlist", false);
                     /*   if(fromsalelist){
                            intent.putExtra("fromsalelist", true);
                        }
                        else{
                            intent.putExtra("fromsalelist", false);
                        }*/
                startActivity(intent);
                finish();
                  /*  Intent returnIntent = new Intent();
                    returnIntent.putExtra("name", adapter.getItem(i));
                    String id = idList.get(getPositionOfItem(adapter.getItem(i)));
                    returnIntent.putExtra("id", id);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();*/
            }
        });
        /*if (isDirectForItem){
            autoCompleteTextView.setVisibility(View.GONE);
        }*/
    }

    private int getPositionOfItem(String category) {
        for (int i = 0; i < this.nameList.size(); i++) {
            if (this.nameList.get(i) == category)
                return i;
        }

        return -1;
    }

    @Subscribe
    public void timout(String msg) {
        snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
        mProgressDialog.dismiss();
    }

    public int taxSplit(String tax) {
        int a = 0;
        if (tax.contains("GST ")) {
            String[] arr = tax.split(" ");
            String[] arr1 = arr[1].split("%");
            a = Integer.parseInt(arr1[0]);
        }
        return a;
    }

  /*  @Subscribe
    public void getVoucherNumber(VoucherSeriesResponse response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {
            for (int i = 0; i < response.getVoucher_series().getData().size(); i++) {
                if (response.getVoucher_series().getData().get(i).getAttributes().isDefaults()) {
                    Preferences.getInstance(getApplicationContext()).setVoucherSeries(response.getVoucher_series().getData().get(i).getAttributes().getName());
                    Preferences.getInstance(getApplicationContext()).setVoucher_number(response.getVoucher_series().getData().get(i).getAttributes().getVoucher_number());
                }
            }
        } else {
            Helpers.dialogMessage(SaleOrderItemListActivity.this, response.getMessage());
        }
    }*/


    public Double getTotal() {
        String total = mTotal.getText().toString();
        String[] arr = total.split(":");
        Double a = Double.valueOf(arr[1].trim());
        return a;
    }

    void posVoucherDetails() {
        ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_SALE_VOUCHER_DETAILS);
    }

    @Subscribe
    public void getPosVoucherDetails(GetSaleVoucherDetails response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {

            Preferences.getInstance(getApplicationContext()).setPos_date(response.getSale_voucher().getData().getAttributes().getDate());
            FirstPageActivity.pos = response.getSale_voucher().getData().getAttributes().getPos();
            appUser.arr_series.add(response.getSale_voucher().getData().getAttributes().getVoucher_series().getName());
            if (response.getSale_voucher().getData().getAttributes().getVoucher_series().isAuto_increment()) {
                Preferences.getInstance(getApplicationContext()).setAuto_increment("true");
            } else {
                Preferences.getInstance(getApplicationContext()).setAuto_increment("false");
            }
            Preferences.getInstance(getApplicationContext()).setVoucherSeries(response.getSale_voucher().getData().getAttributes().getVoucher_series().getName());
            Preferences.getInstance(getApplicationContext()).setVoucher_number(response.getSale_voucher().getData().getAttributes().getVoucher_series().getVoucher_number());
            Preferences.getInstance(getApplicationContext()).setPos_sale_type(response.getSale_voucher().getData().getAttributes().getSale_type());
            Preferences.getInstance(getApplicationContext()).setPos_sale_type_id("" + response.getSale_voucher().getData().getAttributes().getSale_type_id());
            Preferences.getInstance(getApplicationContext()).setPos_store(response.getSale_voucher().getData().getAttributes().getMaterial_center());
            Preferences.getInstance(getApplicationContext()).setPos_store_id(String.valueOf(response.getSale_voucher().getData().getAttributes().getMaterial_center_id()));
            Preferences.getInstance(getApplicationContext()).setPos_party_id(String.valueOf(response.getSale_voucher().getData().getAttributes().getAccount_master_id()));
            Preferences.getInstance(getApplicationContext()).setPos_party_name(response.getSale_voucher().getData().getAttributes().getAccount_master());
            Preferences.getInstance(getApplicationContext()).setPos_mobile(Helpers.mystring(response.getSale_voucher().getData().getAttributes().getMobile_number()));
            appUser.totalamount = String.valueOf(response.getSale_voucher().getData().getAttributes().getTotal_amount());
            appUser.items_amount = String.valueOf(response.getSale_voucher().getData().getAttributes().getItems_amount());
            appUser.bill_sundries_amount = String.valueOf(response.getSale_voucher().getData().getAttributes().getBill_sundries_amount());
            LocalRepositories.saveAppUser(getApplicationContext(), appUser);
            mTotal.setText("Total : " + response.getSale_voucher().getData().getAttributes().getItems_amount());
            if (response.getSale_voucher().getData().getAttributes().getVoucher_items().size() > 0) {
                for (int i = 0; i < response.getSale_voucher().getData().getAttributes().getVoucher_items().size(); i++) {
                    Map mMap = new HashMap<>();
                    mMap.put("id", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getId()));
                    mMap.put("item_id", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem_id()));
                    mMap.put("item_name", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem());
                    mMap.put("description", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem_description());
                    mMap.put("quantity", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getQuantity()));
                    mMap.put("unit", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem_unit());
                    if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getDiscount() == null) {
                        mMap.put("discount", "0.0");
                    } else {
                        mMap.put("discount", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getDiscount()));
                    }
                    if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPrice() == null) {
                        mMap.put("value", "0.0");
                    } else {
                        mMap.put("value", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPrice()));
                    }
                    mMap.put("default_unit", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getDefault_unit_for_sales());
                    mMap.put("packaging_unit", Helpers.mystring(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_unit()));
                    mMap.put("sales_price_alternate", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSales_price_alternate()));
                    mMap.put("sales_price_main", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSales_price_main()));
                    mMap.put("alternate_unit", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getAlternate_unit());
                    mMap.put("packaging_unit_sales_price", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_unit_sales_price()));
                    mMap.put("main_unit", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem_unit());
                    mMap.put("batch_wise", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getBatch_wise_detail());
                    mMap.put("serial_wise", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSerial_number_wise_detail());
                    if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getBusiness_type() != null) {
                        mMap.put("business_type", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getBusiness_type());
                    }
                    StringBuilder sb = new StringBuilder();
                    for (String str : response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getBarcode()) {
                        sb.append(str).append(","); //separating contents using semi colon
                    }
                    String strfromArrayList = sb.toString();
                    mMap.put("barcode", strfromArrayList);
                    appUser.sale_item_serial_arr = response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getVoucher_barcode();
                    LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                    Timber.i("zzzzz  " + appUser.sale_item_serial_arr.toString());
                    StringBuilder sb1 = new StringBuilder();
                    for (String str : response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getVoucher_barcode()) {
                        sb1.append(str).append(","); //separating contents using semi colon
                    }
                    String strfromArraList1 = sb1.toString().trim();
                    Timber.i("zzzzzz  " + strfromArraList1);
                    mMap.put("voucher_barcode", strfromArraList1);
                    mMap.put("serial_number", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getVoucher_barcode());
                    mMap.put("sale_unit", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSale_unit());
                    ArrayList<String> mUnitList = new ArrayList<>();
                    mUnitList.add("Main Unit : " + response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem_unit());
                    mUnitList.add("Alternate Unit :" + response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getAlternate_unit());
                    if (!Helpers.mystring(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_unit()).equals("")) {
                        mUnitList.add("Packaging Unit :" + Helpers.mystring(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_unit()));
                    }
                    mMap.put("total", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPrice_after_discount()));
                    if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSale_unit() != null) {
                        if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSale_unit().equals(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getItem_unit())) {
                            if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getRate_item() != null) {
                                if (!String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getRate_item()).equals("")) {
                                    mMap.put("rate", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getRate_item()));
                                } else {

                                    mMap.put("rate", "0.0");
                                }
                            } else {
                                if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSales_price_main() != null) {
                                    mMap.put("rate", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSales_price_main()));
                                } else {
                                    mMap.put("rate", "0.0");
                                }
                            }

                            mMap.put("price_selected_unit", "main");
                        } else if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSale_unit().equals(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getAlternate_unit())) {
                            mMap.put("price_selected_unit", "alternate");
                            mMap.put("rate", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSales_price_alternate()));
                        } else if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSale_unit().equals(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_unit())) {
                            mMap.put("rate", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_unit_sales_price()));
                            mMap.put("price_selected_unit", "packaging");
                        } else {
                            if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getRate_item() != null) {
                                mMap.put("rate", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getRate_item()));
                            } else {
                                mMap.put("rate", "0.0");
                            }
                            mMap.put("price_selected_unit", "main");
                        }
                    }

                    mMap.put("alternate_unit_con_factor", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getConversion_factor()));
                    mMap.put("packaging_unit_con_factor", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPackaging_conversion_factor()));
                    mMap.put("mrp", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getMrp()));
                    mMap.put("tax", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getTax_category());
                    if (response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getPurchase_price_applied_on() != null) {
                        mMap.put("applied", response.getSale_voucher().getData().getAttributes().getVoucher_items().get(i).getSale_price_applied_on());
                    } else {
                        mMap.put("applied", "Main Unit");
                    }
                    //   mMap.put("serial_number", appUser.sale_item_serial_arr);
                    mMap.put("unit_list", mUnitList);
                    //  appUser.mListMapForItemSale.add(mMap);
                    SaleOrderItemListActivity.mListMapForItemSale.add(mMap);
                    LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                }
            }

            if (response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries() != null) {

                if (response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().size() > 0) {
                    for (int i = 0; i < response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().size(); i++) {
                        Map mMap = new HashMap<>();
                        mMap.put("id", response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getId());
                        mMap.put("courier_charges", response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getBill_sundry());
                        mMap.put("bill_sundry_id", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getBill_sundry_id()));
                        mMap.put("percentage", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPercentage()));
                        mMap.put("percentage_value", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPercentage()));
                        mMap.put("default_unit", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getDefault_value()));
                        mMap.put("fed_as", response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getAmount_of_bill_sundry_fed_as());
                        mMap.put("fed_as_percentage", response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getBill_sundry_of_percentage());
                        mMap.put("type", response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getBill_sundry_type());
                        mMap.put("amount", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPercentage()));
                        // mMap.put("previous", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPrevious_amount()));
                        if (response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPrevious_amount() != 0.0) {
                            mMap.put("fed_as_percentage", "valuechange");
                            mMap.put("changeamount", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPrevious_amount()));
                            billSundryTotal.add(i, String.format("%.2f", response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getPrevious_amount()));
                        }
                        mMap.put("number_of_bill", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getNumber_of_bill_sundry()));
                        // }
                        /*  if(String.valueOf(true)!=null) {*/
                        mMap.put("consolidated", String.valueOf(response.getSale_voucher().getData().getAttributes().getVoucher_bill_sundries().get(i).getConsolidate_bill_sundry()));

                        //   appUser.mListMapForBillSale.add(mMap);
                        mListMapForBillSale.add(mMap);
                        LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                    }
                }
            }
            if (mListMapForItemSale.size() > 0) {
                mChildCheckStates = new HashMap<Integer, String[]>();
                expListView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
                for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                    expListView.expandGroup(i);
                }
            }
        } else {
            /*snackbar = Snackbar
                    .make(coordinatorLayout, response.getMessage(), Snackbar.LENGTH_LONG);
            snackbar.show();*/
            Helpers.dialogMessage(getApplicationContext(), response.getMessage());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        appUser = LocalRepositories.getAppUser(getApplicationContext());
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                if (ParameterConstant.handleAutoCompleteTextView == 1) {
                    tvShipTo.setText(ParameterConstant.name);
                    Preferences.getInstance(SaleOrderItemListActivity.this).setShipped_to_sale_order_id(ParameterConstant.id);
                    Preferences.getInstance(SaleOrderItemListActivity.this).setShipped_to_sale_order(ParameterConstant.name);

                } else {
                    String result = data.getStringExtra("name");
                    String id = data.getStringExtra("id");
                    String mobile = data.getStringExtra("mobile");
                    String group = data.getStringExtra("group");
                    String[] strArr = result.split(",");
                    tvShipTo.setText(strArr[0]);
                    Preferences.getInstance(SaleOrderItemListActivity.this).setShipped_to_sale_order(strArr[0]);
                    Preferences.getInstance(SaleOrderItemListActivity.this).setShipped_to_sale_order_id(id);
                    return;
                }
            }
        }

        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("name");
                String id = data.getStringExtra("id");
                mSaleType.setText(result);
                Preferences.getInstance(getApplicationContext()).setPos_sale_type(result);
                Preferences.getInstance(getApplicationContext()).setPos_sale_type_id(id);
                mListMapForItemSale.clear();
                listAdapter.notifyDataSetChanged();
            }
        }

        if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {

                if (ParameterConstant.handleAutoCompleteTextView == 1) {
                    mPartyName.setText(ParameterConstant.name);
                    //  mMobileNumber.setText(ParameterConstant.mobile);
                    appUser.sale_partyName = ParameterConstant.id;
                    appUser.sale_partyEmail = ParameterConstant.email;
                    LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                    Preferences.getInstance(getApplicationContext()).setPos_party_id(ParameterConstant.id);
                    Preferences.getInstance(getApplicationContext()).setPos_party_name(ParameterConstant.name);
                    Preferences.getInstance(getApplicationContext()).setPos_mobile(ParameterConstant.mobile);
                    appUser.sale_partyEmail = ParameterConstant.email;
                } else {
                    String result = data.getStringExtra("name");
                    String id = data.getStringExtra("id");
                    String mobile = data.getStringExtra("mobile");
                    String group = data.getStringExtra("group");
                    String[] strArr = result.split(",");
                    mPartyName.setText(strArr[0]);
                    // mMobileNumber.setText(mobile);
                    appUser.sale_partyEmail = strArr[3];
                    LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                    Preferences.getInstance(getApplicationContext()).setPos_party_id(id);
                    Preferences.getInstance(getApplicationContext()).setPos_party_name(strArr[0]);
                    Preferences.getInstance(getApplicationContext()).setPos_mobile(mobile);
                    return;
                }
            }
        }
    }


    @Subscribe
    public void getVoucherNumber(VoucherSeriesResponse response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {
            int pos = -1;
            for (int i = 0; i < response.getVoucher_series().getData().size(); i++) {
                Map map = new HashMap();
                map.put("id", response.getVoucher_series().getData().get(i).getId());
                map.put("name", response.getVoucher_series().getData().get(i).getAttributes().getName());
                if (response.getVoucher_series().getData().get(i).getAttributes().isDefaults()) {
                    pos = i;
                }
                map.put("default", String.valueOf(response.getVoucher_series().getData().get(i).getAttributes().isDefaults()));
                map.put("auto_increment", String.valueOf(response.getVoucher_series().getData().get(i).getAttributes().isAuto_increment()));
                map.put("voucher_number", response.getVoucher_series().getData().get(i).getAttributes().getVoucher_number());
                appUser.arr_series.add(response.getVoucher_series().getData().get(i).getAttributes().getName());
                appUser.series_details.add(map);

            }
            LocalRepositories.saveAppUser(SaleOrderItemListActivity.this, appUser);
            mVoucherAdapter = new ArrayAdapter<String>(this, R.layout.layout_spinner, appUser.arr_series);
            mVoucherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //  mVoucherAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, appUser.arr_series);
            mSeries.setAdapter(mVoucherAdapter);
            mSeries.setSelection(pos);
            mSeries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Map map = new HashMap();
                    map = appUser.series_details.get(position);
                    String auto_increament = (String) map.get("auto_increment");
                    String vch_number = (String) map.get("voucher_number");
                    if (auto_increament.equals("false")) {
                        mVchNumber.setEnabled(true);
                    } else {
                        mVchNumber.setEnabled(false);
                    }
                    mVchNumber.setText(vch_number);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            // mVchNumber.setText(response.get());
        } else {
            // Snackbar.make(coordinatorLayout, response.getMessage(), Snackbar.LENGTH_LONG).show();
            // set_date.setOnClickListener(this);
            Helpers.dialogMessage(SaleOrderItemListActivity.this, response.getMessage());
        }
    }
}


