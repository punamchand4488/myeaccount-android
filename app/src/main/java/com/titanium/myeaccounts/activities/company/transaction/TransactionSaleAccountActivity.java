package com.titanium.myeaccounts.activities.company.transaction;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.app.ConnectivityReceiver;
import com.titanium.myeaccounts.activities.app.RegisterAbstractActivity;
import com.titanium.myeaccounts.activities.company.navigations.administration.masters.account.ExpandableAccountListActivity;
import com.titanium.myeaccounts.adapters.AccountExpandableListAdapter;
import com.titanium.myeaccounts.adapters.TransactionSaleAccountExpandableListAdapter;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.networks.ApiCallsService;
import com.titanium.myeaccounts.networks.api_response.account.GetAccountResponse;
import com.titanium.myeaccounts.utils.Cv;
import com.titanium.myeaccounts.utils.Helpers;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.ParameterConstant;
import com.titanium.myeaccounts.utils.TypefaceCache;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class TransactionSaleAccountActivity extends RegisterAbstractActivity {

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.lvExp)
    ExpandableListView expListView;
    @BindView(R.id.floating_button)
    FloatingActionButton mFloatingButton;
    @BindView(R.id.search_view)
    AutoCompleteTextView autoCompleteTextView;
    @BindView(R.id.top_layout)
    RelativeLayout mOverlayLayout;
    @BindView(R.id.error_layout)
    LinearLayout error_layout;
    ProgressDialog mProgressDialog;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Helpers.initActionbar(this,getSupportActionBar(),"ACCOUNT LIST");
//        setContentView(R.layout.activity_transaction_sale_account);
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_transaction_sale_account;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUser appUser=LocalRepositories.getAppUser(this);
        appUser.account_master_group = "Sundry Debtors,Sundry Creditors,Cash-in-hand";
        LocalRepositories.saveAppUser(this,appUser);
        Boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            mProgressDialog = new ProgressDialog(TransactionSaleAccountActivity.this);
            mProgressDialog.setMessage("Info...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
            ApiCallsService.action(getApplicationContext(), Cv.ACTION_GET_ACCOUNT);


        } else {
            snackbar = Snackbar
                    .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Boolean isConnected = ConnectivityReceiver.isConnected();
                            if (isConnected) {
                                snackbar.dismiss();
                            }
                        }
                    });
            snackbar.show();
        }
    }



    @Subscribe
    public void getAccount(GetAccountResponse response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {

            if (response.getOrdered_accounts().size() == 0) {
                expListView.setVisibility(View.GONE);
                error_layout.setVisibility(View.VISIBLE);
            }else {
                expListView.setVisibility(View.VISIBLE);
                error_layout.setVisibility(View.GONE);
            }

            expListView.setAdapter(new TransactionSaleAccountExpandableListAdapter(getApplicationContext(),response.getOrdered_accounts()));


        } else {
            Helpers.dialogMessage(this,response.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
