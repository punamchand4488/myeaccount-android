package com.titanium.myeaccounts.activities.user;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.app.ConnectivityReceiver;
import com.titanium.myeaccounts.activities.app.RegisterAbstractActivity;
import com.titanium.myeaccounts.activities.company.CompanyListActivity;
import com.titanium.myeaccounts.adapters.PackagesItemAdapter;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.networks.ApiCallsService;
import com.titanium.myeaccounts.networks.api_response.packages.GetPackageResponse;
import com.titanium.myeaccounts.networks.api_response.packages.PlanResponse;
import com.titanium.myeaccounts.utils.Cv;
import com.titanium.myeaccounts.utils.Helpers;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.Preferences;
import com.titanium.myeaccounts.utils.TypefaceCache;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PackageActivity extends RegisterAbstractActivity {
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    ArrayList<String> mSpinnerPackageList;
    ArrayList<String> mSpinnerPackageListAmount;
    ArrayList<String> mSpinnerPackageListId;
    ArrayList<String> arrPackagePrice;
    ArrayList<String> arrItemsMessage;
    ArrayList<String> arrItemsName;
    ArrayList<ArrayList<String>> arrTotalItemsName;
    ArrayList<ArrayList<String>> arrTotalItemsMessage;
    ArrayAdapter<String> spinnerAdapter;
    ArrayAdapter<String> spinnerAdapterAmount;
    @BindView(R.id.layout_popular)
    LinearLayout mPopularLayout;
    @BindView(R.id.spinner_package)
    Spinner mSpinnerPackage;
    @BindView(R.id.spinner_amount)
    Spinner mSpinnerAmount;
    ProgressDialog mProgressDialog;
    AppUser appUser;
    Snackbar snackbar;
    PackagesItemAdapter mAdapter;
    @BindView(R.id.list_view_item)
    ListView mItemList;
    @BindView(R.id.packageAmount)
    TextView mPackageAmount;
    int position;
    GetPackageResponse response;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initActionbar();
        appUser = LocalRepositories.getAppUser(this);
        Boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            mProgressDialog = new ProgressDialog(PackageActivity.this);
            mProgressDialog.setMessage("Info...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
            ApiCallsService.action(this, Cv.ACTION_GET_PACKAGES);
        } else {
            snackbar = Snackbar
                    .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Boolean isConnected = ConnectivityReceiver.isConnected();
                            if (isConnected) {
                                snackbar.dismiss();
                            }
                        }
                    });
            snackbar.show();
        }


    }

    public void submit(View v) {
        Dialog dialogbal = new Dialog(PackageActivity.this);
        dialogbal.setContentView(R.layout.dialog_package_serial_number);
        dialogbal.setCancelable(true);
        EditText serial = (EditText) dialogbal.findViewById(R.id.serial);
        RadioGroup radiogrp = (RadioGroup) dialogbal.findViewById(R.id.radioGroup);
        RadioButton cash = (RadioButton) dialogbal.findViewById(R.id.radioButtonCash);
        RadioButton cheque = (RadioButton) dialogbal.findViewById(R.id.radioButtonCheque);
        LinearLayout submit = (LinearLayout) dialogbal.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appUser.package_serail_number = serial.getText().toString();
                appUser.package_mode = String.valueOf(radiogrp.getCheckedRadioButtonId());
                LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                Boolean isConnected = ConnectivityReceiver.isConnected();
                if (isConnected) {
                    mProgressDialog = new ProgressDialog(PackageActivity.this);
                    mProgressDialog.setMessage("Info...");
                    mProgressDialog.setIndeterminate(false);
                    mProgressDialog.setCancelable(true);
                    mProgressDialog.show();
                    ApiCallsService.action(getApplicationContext(), Cv.ACTION_PLANS);
                } else {
                    snackbar = Snackbar
                            .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Boolean isConnected = ConnectivityReceiver.isConnected();
                                    if (isConnected) {
                                        snackbar.dismiss();
                                    }
                                }
                            });
                    snackbar.show();
                }
                dialogbal.dismiss();
            }
        });
        dialogbal.show();

    }


    @Override
    protected int layoutId() {
        return R.layout.activity_package;
    }

    private void initActionbar() {
        ActionBar actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar_tittle_text_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#067bc9")));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(viewActionBar, params);
        TextView actionbarTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
        actionbarTitle.setText("CHOOSE PACKAGE");
        actionbarTitle.setTextSize(16);
        actionbarTitle.setTypeface(TypefaceCache.get(getAssets(), 3));
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Subscribe
    public void getpackage(GetPackageResponse response) {
        this.response=response;
        mProgressDialog.dismiss();
        mSpinnerPackageList = new ArrayList<String>();
        mSpinnerPackageListAmount = new ArrayList<String>();
        mSpinnerPackageListAmount.add("yearly_price");
        mSpinnerPackageListAmount.add("monthly_price");
        mSpinnerPackageListAmount.add("six_month_price");
        mSpinnerPackageListAmount.add("five_year_price");

        mSpinnerPackageListId = new ArrayList<String>();
        arrPackagePrice = new ArrayList<>();
        arrTotalItemsName = new ArrayList<>();
        arrTotalItemsMessage = new ArrayList<>();
        if (response.getStatus() == 200) {
            for (int i = 0; i < response.getPlan().getData().size(); i++) {
                mSpinnerPackageListId.add(response.getPlan().getData().get(i).getId());
                mSpinnerPackageList.add(response.getPlan().getData().get(i).getAttributes().getName());
                arrPackagePrice.add(String.valueOf(response.getPlan().getData().get(i).getAttributes().getYearly_price()));
                arrItemsName = new ArrayList<>();
                arrItemsMessage = new ArrayList<>();
                for (int j = 0; j < response.getPlan().getData().get(i).getAttributes().getFeatures().size(); j++) {
                    arrItemsName.add(response.getPlan().getData().get(i).getAttributes().getFeatures().get(j).getName());
                    arrItemsMessage.add(response.getPlan().getData().get(i).getAttributes().getFeatures().get(j).getDescription());

                }
                arrTotalItemsName.add(arrItemsName);
                arrTotalItemsMessage.add(arrItemsMessage);
            }


            spinnerAdapter = new ArrayAdapter<String>(this,
                    R.layout.layout_trademark_type_spinner_dropdown_item, mSpinnerPackageList);
            spinnerAdapter.setDropDownViewResource(R.layout.layout_trademark_type_spinner_dropdown_item);
            mSpinnerPackage.setAdapter(spinnerAdapter);


            spinnerAdapterAmount = new ArrayAdapter<String>(this,
                    R.layout.layout_trademark_type_spinner_dropdown_item, mSpinnerPackageListAmount);
            spinnerAdapterAmount.setDropDownViewResource(R.layout.layout_trademark_type_spinner_dropdown_item);
            mSpinnerAmount.setAdapter(spinnerAdapterAmount);

            mSpinnerPackage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        position=i;
                        mSpinnerAmount.setSelection(0);
                        appUser.package_id = mSpinnerPackageListId.get(i);
                        appUser.package_amount = arrPackagePrice.get(i);
                        LocalRepositories.saveAppUser(getApplicationContext(), appUser);
                        mPackageAmount.setText(arrPackagePrice.get(i));
                        mAdapter = new PackagesItemAdapter(PackageActivity.this, response.getPlan().getData().get(i).getAttributes().getFeatures());
                        mItemList.setAdapter(mAdapter);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            mSpinnerAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i==0) {
                        mPackageAmount.setText(String.valueOf(response.getPlan().getData().get(position).getAttributes().getYearly_price()));
                    }else  if (i==1) {
                        mPackageAmount.setText(String.valueOf(response.getPlan().getData().get(position).getAttributes().getMonthly_price()));
                    }else  if (i==2) {
                        mPackageAmount.setText(String.valueOf(response.getPlan().getData().get(position).getAttributes().getSix_month_price()));
                    }else  if (i==3) {
                        mPackageAmount.setText(String.valueOf(response.getPlan().getData().get(position).getAttributes().getFive_year_price()));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            Helpers.dialogMessage(this, response.getMessage());
        }
    }

    @Subscribe
    public void plans(PlanResponse response) {
        mProgressDialog.dismiss();
        if (response.getStatus() == 200) {
            Preferences.getInstance(getApplicationContext()).setLogin(true);
            Intent intent = new Intent(getApplicationContext(), CompanyListActivity.class);
            startActivity(intent);
        } else {
            Helpers.dialogMessage(this, response.getMessage());
        }
    }

    @Subscribe
    public void timout(String msg) {
        snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
        mProgressDialog.dismiss();

    }
}