package com.titanium.myeaccounts.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.company.pos.PosItemAddActivity;
import com.titanium.myeaccounts.activities.company.salepos.SaleOrderItemListActivity;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.utils.EventForPos;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.Preferences;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by BerylSystems on 11/25/2017.
 */

public class SaleOrderAddItemsAdapter extends RecyclerView.Adapter<SaleOrderAddItemsAdapter.ViewHolder> {

    Context context;
    List<Map<String,String>> mListMap;
    Double mInteger = 0.0;
    AppUser appUser;
    public Map mMapPosItem;
    Boolean mBool = true;
    //ViewHolder holder;

    public SaleOrderAddItemsAdapter(Context context, List<Map<String,String>> mListMap) {
        this.context = context;
        this.mListMap = mListMap;
        appUser = LocalRepositories.getAppUser(context);
    }

    @Override
    public SaleOrderAddItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_for_sale_order, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mListMap.size();
    }

    @Override
    public void onBindViewHolder(SaleOrderAddItemsAdapter.ViewHolder viewHolder, int position) {
        // appUser = LocalRepositories.getAppUser(context);
        mInteger = 0.0;
        mBool = true;
        Map map = mListMap.get(position);
        String item_id = (String) map.get("item_id");
        String itemName = (String) map.get("item_name");
        String quantity = (String) map.get("quantity");
        String item_tax = (String) map.get("tax");
        Double item_amount = Double.parseDouble((String) map.get("rate"));
        Double total = Double.valueOf((String) map.get("total"));
        String tax = (String) map.get("tax").toString();
        viewHolder.mItemName.setText(itemName);
        viewHolder.mQuantity.setText("qty: "+quantity);
        viewHolder.mItemAmount.setText("₹ " + String.format("%.2f", item_amount));
        viewHolder.mItemTotal.setText("₹ " + String.format("%.2f", total));
        viewHolder.mItemTax.setText(item_tax);
    }

    public void setTotal(String amount, Boolean mBool, Double gst, Double taxValue, String tax) {
        Double total = 0.0, grandTotal = 0.0;
        String taxString = Preferences.getInstance(context).getPos_sale_type();
        appUser = LocalRepositories.getAppUser(context);
        if (mBool) {
            total = getTotal(PosItemAddActivity.mSubtotal.getText().toString()) + Double.valueOf(amount);
            grandTotal = getTotal(PosItemAddActivity.grand_total.getText().toString()) + Double.valueOf(amount);
        } else {
            total = getTotal(PosItemAddActivity.mSubtotal.getText().toString()) - Double.valueOf(amount);
            grandTotal = getTotal(PosItemAddActivity.grand_total.getText().toString()) - Double.valueOf(amount);
        }

        PosItemAddActivity.mSubtotal.setText("₹ " + String.format("%.2f", total));
        PosItemAddActivity.grand_total.setText("₹ " + String.format("%.2f", grandTotal));
        //setTaxChange(context, total, gst, taxValue, tax, mBool);
        //  appUser = LocalRepositories.getAppUser(context);
        if (SaleOrderItemListActivity.mListMapForBillSale.size() > 0) {
            if (taxString.contains("GST-MultiRate")) {
                String subTotal = String.valueOf(total) + "," + String.valueOf(gst) + "," + String.valueOf(taxValue) + "," + String.valueOf(mBool);
                EventBus.getDefault().post(new EventForPos(subTotal));
            } else {
                String subTotal = String.valueOf(total);
                EventBus.getDefault().post(new EventForPos(subTotal));
            }
        } else {
            granTotal(total, 0.00/*taxSplit(tax)*/);
        }
    }

    public Double getTotal(String total) {
        String[] arr = total.split("₹ ");
        Double a = Double.valueOf(arr[1].trim());
        return a;
    }

    public static void granTotal(Double subtotal, Double tax) {
        Double grandTotal = subtotal + tax;
        PosItemAddActivity.grand_total.setText("₹ " + String.format("%.2f", grandTotal));
    }

    public Double taxSplit(String tax) {
        Double a = 0.0;
        if (tax.contains("GST ")) {
            String[] arr = tax.split(" ");
            String[] arr1 = arr[1].split("%");
            a = Double.valueOf(arr1[0]);
        }
        return a;
    }

    public static Double getMultiRateTaxChange(String total) {
        // String total = PosItemAddActivity.mSubtotal.getText().toString();
        if (total.equals("0.0")) {
            total = "₹ 0.0";
        }
        String[] arr = total.split("₹ ");
        Double a = Double.valueOf(arr[1].trim());
        return a;
    }

    public static Double gstPlusMinus(Double gst_total, Double gst, Boolean mBool) {
        Double total = 0.0;
        if (mBool) {
            total = gst_total + gst;
        } else {
            total = gst_total - gst;
        }
        return total;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lblListItem)
        TextView mItemName;
        @BindView(R.id.quantity)
        TextView mQuantity;
        @BindView(R.id.item_amount)
        TextView mItemAmount;
        @BindView(R.id.item_total)
        TextView mItemTotal;
        @BindView(R.id.item_tax)
        TextView mItemTax;
        @BindView(R.id.decrease)
        LinearLayout decrease;
        @BindView(R.id.increase)
        LinearLayout increase;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
