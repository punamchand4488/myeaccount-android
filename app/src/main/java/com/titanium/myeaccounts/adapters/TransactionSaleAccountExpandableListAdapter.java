package com.titanium.myeaccounts.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.networks.api_response.account.GetAccountResponse;
import com.titanium.myeaccounts.networks.api_response.account.OrderedAccounts;
import com.titanium.myeaccounts.utils.EventAccountChildClicked;
import com.titanium.myeaccounts.utils.EventDeleteAccount;
import com.titanium.myeaccounts.utils.EventEditAccount;
import com.titanium.myeaccounts.utils.EventSelectAccountPurchase;
import com.titanium.myeaccounts.utils.ParameterConstant;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TransactionSaleAccountExpandableListAdapter extends BaseExpandableListAdapter {
    ArrayList<OrderedAccounts> responseList;
    private Context _context;

    public TransactionSaleAccountExpandableListAdapter(Context context, ArrayList<OrderedAccounts> responseList) {
        this._context = context;
        this.responseList=responseList;
    }



    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {



        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView amount = (TextView) convertView.findViewById(R.id.amount_qty);
        name.setText(responseList.get(groupPosition).getData().get(childPosition).getAttributes().getName());
        amount.setText("₹ " +String.format("%.2f", responseList.get(groupPosition).getData().get(childPosition).getAttributes().getAmount()));
        LinearLayout delete = (LinearLayout) convertView.findViewById(R.id.delete_icon);
        LinearLayout edit = (LinearLayout) convertView.findViewById(R.id.edit_icon);
        LinearLayout mMainLayout = (LinearLayout) convertView.findViewById(R.id.main_layout);
        //LinearLayout mainLayout = (LinearLayout) convertView.findViewById(R.id.mainLayout);

        if (responseList.get(groupPosition).getData().get(childPosition).getAttributes().getUndefined().equals("true")) {
            delete.setVisibility(View.VISIBLE);
            edit.setVisibility(View.VISIBLE);
        } else {
            delete.setVisibility(View.INVISIBLE);
            edit.setVisibility(View.INVISIBLE);
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = groupPosition + "," + childPosition;
                EventBus.getDefault().post(new EventDeleteAccount(id));
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = groupPosition + "," + childPosition;
                EventBus.getDefault().post(new EventEditAccount(id));
            }
        });
        mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = groupPosition + "," + childPosition;
                EventBus.getDefault().post(new EventAccountChildClicked(id));
                EventBus.getDefault().post(new EventSelectAccountPurchase(id));
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.responseList.get(groupPosition).getData().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.responseList.get(groupPosition);
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return this.responseList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
       /* if(!ParameterConstant.forPaymentSettlement.equals("")&&headerTitle.equals("Sundry Debtors")){

        }*/

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(responseList.get(groupPosition).getGroup_name());
        if(ParameterConstant.forPaymentSettlement!=null) {
            if (!ParameterConstant.forPaymentSettlement.equals("")) {
                if (responseList.get(groupPosition).getGroup_name().equals("Sundry Debtors")) {
                    ImageView imageview = (ImageView) convertView.findViewById(R.id.image);
                    imageview.setVisibility(View.GONE);
                } else {
                    ImageView imageview = (ImageView) convertView.findViewById(R.id.image);
                    if (isExpanded) {
                        imageview.setImageResource(R.drawable.up_arrow);
                    } else {
                        imageview.setImageResource(R.drawable.down_arrow);
                    }
                }
            }
        }
        else{
            ImageView imageview = (ImageView) convertView.findViewById(R.id.image);
            if (isExpanded) {
                imageview.setImageResource(R.drawable.up_arrow);
            } else {
                imageview.setImageResource(R.drawable.down_arrow);
            }
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}