package com.titanium.myeaccounts.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.networks.api_response.serialnumber.Data;
import com.titanium.myeaccounts.utils.EventDeleteSerialNumber;
import com.titanium.myeaccounts.utils.EventShowPdf;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SerialNumberReferenceAdapter extends RecyclerView.Adapter<SerialNumberReferenceAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Data> data;

    public SerialNumberReferenceAdapter(Context context, ArrayList<Data> data) {
        this.context = context;
        this.data = data;

    }

    @Override
    public SerialNumberReferenceAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_serial_number_reference, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if(data.get(position).getAttributes().getType().equals("Item Opening Stock")){
            viewHolder.mMainLayout.setVisibility(View.GONE);
            viewHolder.mOpeningStockLayout.setVisibility(View.VISIBLE);
            viewHolder.mItemName.setText(data.get(position).getAttributes().getName());
            viewHolder.mItemDate.setText(data.get(position).getAttributes().getDate());
            viewHolder.mOpeningStockType.setText(data.get(position).getAttributes().getType());
            viewHolder.mDeleteIcon.setVisibility(View.GONE);
            viewHolder.mIconEye.setVisibility(View.GONE);
            viewHolder.mIconPrinting.setVisibility(View.GONE);
            viewHolder.mIconShare.setVisibility(View.GONE);

        }
        else{
            viewHolder.mMainLayout.setVisibility(View.VISIBLE);
            viewHolder.mOpeningStockLayout.setVisibility(View.GONE);
            viewHolder.mVoucherType.setText(data.get(position).getAttributes().getType());
            viewHolder.mDate.setText(data.get(position).getAttributes().getDate());
            viewHolder.amount.setText(String.valueOf(data.get(position).getAttributes().getAmount()));
            viewHolder.mName.setText(data.get(position).getAttributes().getName());
            viewHolder.mVoucher_no.setText(data.get(position).getAttributes().getVoucher_number());
        }



        viewHolder.mDeleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventDeleteSerialNumber(data.get(position).getAttributes().getType()+","+String.valueOf(data.get(position).getAttributes().getId()) ));
            }
        });
        viewHolder.mIconEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventShowPdf(data.get(position).getAttributes().getType()+","+String.valueOf(data.get(position).getAttributes().getId())));
            }
        });

        viewHolder.mIconPrinting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventShowPdf(data.get(position).getAttributes().getType()+","+String.valueOf(data.get(position).getAttributes().getId())));
            }
        });

        viewHolder.mIconShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new EventShowPdf(data.get(position).getAttributes().getType()+","+String.valueOf(data.get(position).getAttributes().getId())));
            }
        });



    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.voucher_type)
        TextView mVoucherType;
        @BindView(R.id.date)
        TextView mDate;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.voucher_no)
        TextView mVoucher_no;
        @BindView(R.id.name)
        TextView mName;
        @BindView(R.id.icon_delete)
        LinearLayout mDeleteIcon;
        @BindView(R.id.icon_eye)
        LinearLayout mIconEye;
        @BindView(R.id.icon_printing)
        LinearLayout mIconPrinting;
        @BindView(R.id.icon_share)
        LinearLayout mIconShare;
        @BindView(R.id.opening_stock_layout)
        LinearLayout mOpeningStockLayout;
        @BindView(R.id.mainLayout)
        LinearLayout mMainLayout;
        @BindView(R.id.itemname)
        TextView mItemName;
        @BindView(R.id.itemdate)
        TextView mItemDate;
        @BindView(R.id.item_opening_stock)
        TextView mOpeningStockType;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}