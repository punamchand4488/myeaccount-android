package com.titanium.myeaccounts.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.company.salepos.SaleOrderItemListActivity;
import com.titanium.myeaccounts.utils.EventDeleteItem;
import com.titanium.myeaccounts.utils.EventEditItem;
import com.titanium.myeaccounts.utils.EventSaleAddItem;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

public class SaleOrderListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    private HashMap<Integer, List<String>> listDataChildSalePriceMain;
    Double total = 0.0;
    Double sale_price_main = 0.0;

    public SaleOrderListAdapter(Context context, List<String> listDataHeader,
                                HashMap<String, List<String>> listChildData,
                                HashMap<Integer, List<String>> listDataChildSalePriceMain) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.listDataChildSalePriceMain = listDataChildSalePriceMain;
        SaleOrderItemListActivity.mChildCheckStates = new HashMap<Integer, String[]>();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    String childName;
    float mInteger = 0;

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final int mGroupPosition = groupPosition;
        final int mChildPosition = childPosition;
        mInteger = 0;
        final String childText = (String) getChild(groupPosition, childPosition);
        String arr[] = childText.split(",");
        String name = arr[0];
        String quantity = arr[1];
        String item_id = arr[3];
        String itemTax = arr[4];
        sale_price_main = 0.0;
        if (arr[2] != null && !arr[2].equals("")) {
            sale_price_main = Double.valueOf(arr[2]);
        } else {
            sale_price_main = 0.0;
        }
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_for_sale_order, null);
        }
        LinearLayout mainLayout = (LinearLayout) convertView.findViewById(R.id.main_layout);
        LinearLayout addButtonLayout = (LinearLayout) convertView.findViewById(R.id.addButtonLayout);

        TextView txtListChild = (TextView) convertView.findViewById(R.id.posListItem);
        TextView mQuantity = (TextView) convertView.findViewById(R.id.quantity);
        TextView mItemAmount = (TextView) convertView.findViewById(R.id.item_amount);
        TextView mItemTotal = (TextView) convertView.findViewById(R.id.item_total);
        LinearLayout plus_minus_layout = (LinearLayout) convertView.findViewById(R.id.plus_minus_layout);
        LinearLayout add_layout = (LinearLayout) convertView.findViewById(R.id.add_layout);
        LinearLayout decrease = (LinearLayout) convertView.findViewById(R.id.decrease);
        LinearLayout increase = (LinearLayout) convertView.findViewById(R.id.increase);
        TextView itemTaxTxt = (TextView) convertView.findViewById(R.id.item_tax);

        mItemAmount.setText("₹ " + sale_price_main);
        mItemTotal.setText("₹ 0.0");
        itemTaxTxt.setText(itemTax);
        mItemTotal.setVisibility(View.GONE);
        txtListChild.setText(name);
        if (SaleOrderItemListActivity.mListMapForItemSale.size() > 0) {
            for (int i = 0; i < SaleOrderItemListActivity.mListMapForItemSale.size(); i++) {
                String item_id_child = SaleOrderItemListActivity.mListMapForItemSale.get(i).get("item_id").toString();
                if (item_id.equals(item_id_child)) {
                    String pos = groupPosition + "," + childPosition;
                    // mQuantity.setText(SaleOrderItemListActivity.mListMapForItemSale.get(i).get("quantity").toString());
                       /* SaleOrderItemListActivity.mMapPosItem.put(pos, mQuantity.getText().toString());
                        String getChecked[] = SaleOrderItemListActivity.mChildCheckStates.get(mGroupPosition);
                        getChecked[mChildPosition] = mQuantity.getText().toString();
                        SaleOrderItemListActivity.mChildCheckStates.put(mGroupPosition, getChecked);*/


                    mItemAmount.setText("₹ " + SaleOrderItemListActivity.mListMapForItemSale.get(i).get("rate").toString());
                    mItemTotal.setText("₹ " + ""+SaleOrderItemListActivity.mListMapForItemSale.get(i).get("tempTotal"));
                   // total = total + Double.parseDouble(SaleOrderItemListActivity.mListMapForItemSale.get(i).get("total").toString());
                    mItemTotal.setVisibility(View.VISIBLE);
                    //  SaleOrderItemListActivity.mTotal.setText(""+total);
                }
            }
        }

        addButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaleOrderItemListActivity.comingFrom = 101;
                String id = groupPosition + "," + childPosition;
                EventBus.getDefault().post(new EventSaleAddItem(id));
            }
        });

       /* if (this._listDataHeader.size() - 1 == mGroupPosition && this._listDataChild.get(this._listDataHeader.get(groupPosition)).size() - 1 == mChildPosition) {
            SaleOrderItemListActivity.mListMapForItemSale.clear();
        }*/

            /*mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = groupPosition + "," + childPosition;
                    EventBus.getDefault().post(new EventEditItem(id));
                }
            });*/


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        ImageView imageview = (ImageView) convertView.findViewById(R.id.image);

       /* if (comingFromPOS==6 &&  !SaleOrderItemListActivity.isDirectForItem){
            imageview.setVisibility(View.GONE);
            expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    //  Log.d("onGroupClick:", "worked");
                    parent.expandGroup(groupPosition);
                    return true;
                }
            });
        }*/

        if (isExpanded) {
            imageview.setImageResource(R.drawable.up_arrow);
        } else {
            imageview.setImageResource(R.drawable.down_arrow);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

 /*   public void setTotal(String amount, Boolean mBool) {
        Double total = 0.0;
        if (mBool) {
            total = getTotal() + Double.valueOf(amount);
        } else {
            total = getTotal() - Double.valueOf(amount);
        }
       // SaleOrderItemListActivity.mTotal.setText("Total : " + total);
    }

    public Double getTotal() {
        String total = _context.mTotal.getText().toString();
        String[] arr = total.split(":");
        Double a = Double.valueOf(arr[1].trim());
        return a;
    }*/

    public String splitString(String total) {
        // String total = PosItemAddActivity.mSubtotal.getText().toString();
        if (total.equals("0.0")) {
            total = "₹ 0.0";
        }
        String[] arr = total.split("₹ ");
        String a = arr[1].trim();
        return a;
    }
}