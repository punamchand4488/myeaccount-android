package com.titanium.myeaccounts.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.titanium.myeaccounts.R;
import com.titanium.myeaccounts.activities.company.navigations.TransactionPdfActivity;
import com.titanium.myeaccounts.activities.company.transaction.stocktransfer.CreateStockTransferActivity;
import com.titanium.myeaccounts.entities.AppUser;
import com.titanium.myeaccounts.networks.api_response.stocktransfer.Data;
import com.titanium.myeaccounts.utils.EventDeleteStockTransfer;
import com.titanium.myeaccounts.utils.LocalRepositories;
import com.titanium.myeaccounts.utils.Preferences;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;

public class GetStockTransferListAdapter extends RecyclerView.Adapter<GetStockTransferListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Data> data;

    public GetStockTransferListAdapter(Context context, ArrayList<Data> data){
        this.context=context;
        this.data=data;
    }

    @Override
    public GetStockTransferListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_stock_transfer_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GetStockTransferListAdapter.ViewHolder viewHolder, int position) {

        viewHolder.bank_edit_text1.setText(data.get(position).getAttributes().getMaterial_center_from());
        viewHolder.bank_edit_text5.setText(data.get(position).getAttributes().getMaterial_center_to());
        viewHolder.bank_edit_text2.setText(""+String.format("%.2f",data.get(position).getAttributes().total_amount));
        viewHolder.bank_edit_text3.setText(data.get(position).getAttributes().date);
        viewHolder.bank_edit_text4.setText(data.get(position).getAttributes().voucher_number);

        viewHolder.icon_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stock_transfer_voucher_id=data.get(position).getId();
                EventBus.getDefault().post(new EventDeleteStockTransfer(stock_transfer_voucher_id));
            }
        });

        viewHolder.icon_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(position).getAttributes().getInvoice_html()!=null) {
                    Intent intent = new Intent(context, TransactionPdfActivity.class);
                    intent.putExtra("company_report", data.get(position).getAttributes().getInvoice_html());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "PDF not found!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        viewHolder.icon_printing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(position).getAttributes().getInvoice_html()!=null) {
                    Intent intent = new Intent(context, TransactionPdfActivity.class);
                    intent.putExtra("company_report", data.get(position).getAttributes().getInvoice_html());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "PDF not found!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        viewHolder.icon_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(position).getAttributes().getInvoice_html()!=null) {
                    Intent intent = new Intent(context, TransactionPdfActivity.class);
                    intent.putExtra("company_report", data.get(position).getAttributes().getInvoice_html());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "PDF not found!", Toast.LENGTH_SHORT).show();
                }
            }
        });

       /* viewHolder.mAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!data.get(position).getAttributes().getAttachment().equals("")){
                    Intent intent = new Intent(context, ImageOpenActivity.class);
                    intent.putExtra("attachment",data.get(position).getAttributes().getAttachment());
                    intent.putExtra("booleAttachment",true);
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "Attachment not found!", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        viewHolder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.getInstance(context).setUpdate("1");
                AppUser appUser= LocalRepositories.getAppUser(context);
                appUser.edit_stock_transfer_id=data.get(position).getId();
                LocalRepositories.saveAppUser(context,appUser);
                Intent intent = new Intent(context, CreateStockTransferActivity.class);
                intent.putExtra("fromstocklist",true);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bank_edit_text1)
        TextView bank_edit_text1;
        @BindView(R.id.bank_edit_text2)
        TextView bank_edit_text2;
        @BindView(R.id.bank_edit_text3)
        TextView bank_edit_text3;
        @BindView(R.id.bank_edit_text4)
        TextView bank_edit_text4;
        @BindView(R.id.bank_edit_text5)
        TextView bank_edit_text5;
        @BindView(R.id.icon_delete)
        LinearLayout icon_delete;
        @BindView(R.id.icon_eye)
        LinearLayout icon_eye;
        @BindView(R.id.icon_printing)
        LinearLayout icon_printing;
        @BindView(R.id.icon_share)
        LinearLayout icon_share;
        @BindView(R.id.attachment_layout)
        LinearLayout mAttachment;
        @BindView(R.id.mainLayout)
        LinearLayout mMainLayout;
       /*
        @BindView(R.id.edit1)
        LinearLayout mEdit;*/



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}